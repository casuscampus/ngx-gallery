import {} from 'jasmine';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Renderer, SimpleChange } from '@angular/core';
import { TestBed, ComponentFixture, async } from '@angular/core/testing';
import { NgxGalleryActionComponent, NgxGalleryPreviewComponent, NgxGalleryArrowsComponent, NgxGalleryBulletsComponent, NgxGalleryHelperService } from './';
import { LogService } from './utils/logging.service';
import { NgxGallerySvgArrowComponent } from './svg/ngx-gallery-svg-arrow.component';
import { NgxGallerySvgIconComponent } from './svg/ngx-gallery-svg-icon.component';
import { NgxGallerySvgPolygonComponent } from './svg/ngx-gallery-svg-polygon.component';
import { NgxGallerySvgTextComponent } from './svg/ngx-gallery-svg-text.component';
import { NgxGalleryVideoComponent } from './video/ngx-gallery-video.component';
import { UtilsScriptComponent } from './utils/utils-script.component';
import { CcMediaService } from './utils/cc-media.service';
import { NgxGalleryImageSvgComponent } from './image/ngx-gallery-image-svg.component';

describe('NgxGalleryPreviewComponent', () => {
    let fixture: ComponentFixture<NgxGalleryPreviewComponent>;
    let comp: NgxGalleryPreviewComponent;
    let el, prevArrow, nextArrow, image, imageWrapper;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                NgxGalleryPreviewComponent,
                NgxGalleryArrowsComponent,
                NgxGalleryBulletsComponent,
                NgxGalleryActionComponent,
                NgxGalleryVideoComponent,
                NgxGalleryImageSvgComponent,
                NgxGallerySvgArrowComponent,
                NgxGallerySvgIconComponent,
                NgxGallerySvgPolygonComponent,
                NgxGallerySvgTextComponent,
                UtilsScriptComponent,
            ],
          providers: [
              NgxGalleryHelperService,
              Renderer,
              { provide: LogService, useValue: {log: (fName: string, msg: string, prioLevel?: number) => null} },
              { provide: CcMediaService, useValue: {isImage: () => true} },
            ]
        })
        .overrideComponent(NgxGalleryPreviewComponent, {
            set: {
                styleUrls: [],
            }
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NgxGalleryPreviewComponent);
        comp = fixture.componentInstance;
        comp.images = [
            'https://picsum.photos/101',
            'https://picsum.photos/102',
            'https://picsum.photos/103',
        ];
        comp.index = 0;
        comp.descriptions = ['1', '2', '3'];
        comp.src = 'https://picsum.photos/101';
        comp.types = [
            'image/jpg',
            'image/jpg',
            'image/jpg',
        ];
        comp.widths = [100, 100, 100];
        comp.heights = [100, 100, 100];
        comp.mtypes = ['image', 'image', 'image'];
        fixture.detectChanges();
        el = fixture.debugElement.nativeElement;
        image = el.querySelector('.ngx-gallery-preview-img');
        imageWrapper = el.querySelector('.ngx-gallery-preview-wrapper');
    });

    it('should create component', () => {
        expect(fixture.componentInstance).toBeTruthy();
    });

    it('should show next image', (done) => {
        fixture.detectChanges();
        comp.open(0);
        comp.loading = false;
        comp.showNext();

        setTimeout(() => {
            fixture.detectChanges();
            expect(image.getAttribute('src')).toEqual('https://picsum.photos/102');
            done();
        }, 1000)
    });

    it('should not show next image if there is no more images', (done) => {
        fixture.detectChanges();
        comp.open(2);
        comp.loading = false;
        comp.showNext();

        setTimeout(() => {
            fixture.detectChanges();
            expect(image.getAttribute('src')).toEqual('https://picsum.photos/103');
            done();
        }, 1000)
    });

    it('should start from the beggining if there is no more images and infinity move is true', (done) => {
        fixture.detectChanges();
        comp.infinityMove = true;
        comp.open(2);
        comp.loading = false;
        comp.showNext();

        setTimeout(() => {
            fixture.detectChanges();
            expect(image.getAttribute('src')).toEqual('https://picsum.photos/101');
            done();
        }, 1000)
    });

    it('should show prev image', (done) => {
        fixture.detectChanges();
        comp.open(1);
        comp.loading = false;
        comp.showPrev();

        setTimeout(() => {
            fixture.detectChanges();
            expect(image.getAttribute('src')).toEqual('https://picsum.photos/101');
            done();
        }, 1000)
    });

    it('should not show prev image if there is no more images', (done) => {
        fixture.detectChanges();
        comp.open(0);
        comp.loading = false;
        comp.showPrev();

        setTimeout(() => {
            fixture.detectChanges();
            expect(image.getAttribute('src')).toEqual('https://picsum.photos/101');
            done();
        }, 1000)
    });

    it('should start from the end if there is no more images and infinity move is true', (done) => {
        fixture.detectChanges();
        comp.infinityMove = true;
        comp.open(0);
        comp.loading = false;
        comp.showPrev();

        setTimeout(() => {
            fixture.detectChanges();
            expect(image.getAttribute('src')).toEqual('https://picsum.photos/103');
            done();
        }, 1000)
    });

    it('should trigger event onOpen', (done) => {
        comp.onOpen.subscribe(() => {
            done()
        });

        fixture.detectChanges();
        comp.open(1);
    });

    it('should trigger event onClose', (done) => {
        comp.onClose.subscribe(() => {
            done()
        });

        fixture.detectChanges();
        comp.open(1);
        comp.close();
    });

    it('should prevent showing images if images arent defined', () => {
        comp.images = undefined;

        expect(comp.canShowNext()).toBeFalsy();
        expect(comp.canShowPrev()).toBeFalsy();
    });

    it('should allow showing prev images if possible', () => {
        comp.mtypes = ['image', 'image', 'audio'];
        comp.open(2);

        expect(comp.canShowPrev()).toBeTruthy();
    });

    it('should allow showing next images if possible', () => {
        comp.open(1);

        expect(comp.canShowNext()).toBeTruthy();
    });

    it('should not allow showing prev images if not possible', () => {
        comp.open(0);

        expect(comp.canShowPrev()).toBeFalsy();
    });

    it('should not allow showing next images if not possible', () => {
        comp.mtypes = ['image', 'image', 'audio'];
        comp.open(1);

        expect(comp.canShowNext()).toBeFalsy();
    });

    it('should stop auto play on moveenter if autoPlayPauseOnHover is true', (done) => {
        comp.autoPlay = true;
        comp.autoPlayInterval = 100;
        comp.autoPlayPauseOnHover = true;
        comp.open(0);
        comp.loading = false;

        imageWrapper.dispatchEvent(new Event('mouseenter'));

        setTimeout(() => {
            fixture.detectChanges();
            expect(image.getAttribute('src')).toEqual('https://picsum.photos/101');
            done();
        }, 1000)
    });

    it('should start auto play on mouseleave if autoPlayPauseOnHover is true', (done) => {
        comp.autoPlay = true;
        comp.autoPlayInterval = 500;
        comp.autoPlayPauseOnHover = true;
        comp.open(0);
        comp.loading = false;

        imageWrapper.dispatchEvent(new Event('mouseenter'));
        imageWrapper.dispatchEvent(new Event('mouseleave'));

        setTimeout(() => {
            fixture.detectChanges();
            expect(image.getAttribute('src')).toEqual('https://picsum.photos/102');
            done();
        }, 1000)
    });

    it('should trigger change event on show next', () => {
        spyOn(comp.onActiveChange, 'emit');

        comp.showNext();
        expect(comp.onActiveChange.emit).toHaveBeenCalled();
    });

    it('should trigger change event on show previous', () => {
        spyOn(comp.onActiveChange, 'emit');
        comp.open(1);
        comp.loading = false;

        comp.showPrev();
        expect(comp.onActiveChange.emit).toHaveBeenCalled();
    });

    it('should emit change events during autoplay', (done) => {
        spyOn(comp.onActiveChange, 'emit');

        comp.autoPlay = true;
        comp.autoPlayInterval = 500;
        comp.autoPlayPauseOnHover = true;
        comp.open(0);
        comp.loading = false;

        imageWrapper.dispatchEvent(new Event('mouseenter'));
        imageWrapper.dispatchEvent(new Event('mouseleave'));

        setTimeout(() => {
            fixture.detectChanges();
            expect(comp.onActiveChange.emit).toHaveBeenCalledTimes(2);
            done();
        }, 1000);
    });

    // it('should close on escape', (done) => {
    //     comp.closeOnEsc = true;
    //     fixture.detectChanges();
    //     comp.open(1);

    //     comp.onClose.subscribe(() => {
    //         done()
    //     });

    //     let event = new KeyboardEvent('keydown', { key: "escape", detail: 27 });

    //     window.dispatchEvent(event);
    // });
})
