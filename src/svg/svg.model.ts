import { Arrow } from './arrow.model';
import { Point } from './point.model';
import { Icon } from './icon.model';
import { Polygon } from './polygon.model';

// TODO: add example svg to ngx-gallery-test app so that this class can be removed
export class SVGHelper {
    //                     pureTextArr[j] = svgHelper.getText(curwidth, curheight, pureTextArr[j]);

    public getText(x: number, y: number, text: string) {
        return new Point(this.getRandomInt(x), this.getRandomInt(y), text);
    }

    public getArrow(x: number, y: number) {
        return new Arrow(new Point(this.getRandomInt(x), this.getRandomInt(y), ''),
                new Point( this.getRandomInt(x), this.getRandomInt(y), ''));
    }

    public getIcon( imgurl: string, link: string, description: string, x: number, y: number, width: number): Icon {
        return new Icon(
            imgurl,
            {id: '10', description: '', height: 10, width: 10, mimeType: 'image/png'},
            description,
            new Point(this.getRandomInt(x), this.getRandomInt(y), ''),
            10,
            10
        );
    }

    public getPolygon( text: string, x: number, y: number, linkIds) {
        let j: number;
        let points: Point [] = [];
        for (j = 0; j < (3 + this.getRandomInt(3)); j++) {
            points[j] = new Point(this.getRandomInt(x), this.getRandomInt(y), '');
        }
        return new Polygon(text, points, this.getTarget(linkIds));
    }

    public getBasePolygon( text: string, x: number, y: number, linkIds) {
        let j: number;
        let points: Point [] = [];
        points[0] = new Point(0, 0, '');
        points[1] = new Point(x, 0, '');
        points[2] = new Point(x, y, '');
        return new Polygon(text, points, this.getTarget(linkIds));
    }

    public getRandomInt(max: number): number {
        return Math.floor(Math.random() * Math.floor(max));
    }

    public getTarget(linkIds) {
        let ids: string [] = [];
        let type: string;
        switch (Math.floor(Math.random() * Math.floor(3))) {
            case 1: // audio
                ids = linkIds.audio;
                type = 'audio';
                break;
            case 2: // video
                ids = linkIds.video;
                type = 'video';
                break;
            case 3: // campus
                ids = linkIds.campus;
                type = 'campus';
                break;
            default: // image
                type = 'image';
                ids = linkIds.image;
        }

        let index = Math.floor(Math.random() * Math.floor(ids.length));
        console.log('getTarget - linkId chosen(' + type + '): ' + ids[index]);
        return  {id: ids[index], description: '', height: 10, width: 10, mimeType: type + '/*'};
    }
}
