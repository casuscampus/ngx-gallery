export class Point {
    posx: number;
    posy: number;
    text: string;

    constructor(
        posx: number,
        posy: number,
        text: string
    ) {
        this.posx = posx;
        this.posy = posy;
        this.text = text;
    }

    public getPosX(): number {
        return this.posx;
    }

    public getPosY(): number {
        return this.posy;
    }

    public getText(): string {
        return this.text;
    }
}
