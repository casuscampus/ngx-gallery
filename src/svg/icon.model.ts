import { IMedium } from '@casus-campus/cc-support';

import { Point } from './point.model';

export class Icon  {
    iconImageUrl: string;
    target: IMedium;
    description: string;
    point: Point;
    width: number;
    height: number;

    constructor(
        iconImageUrl: string,
        target: IMedium,
        description: string,
        point: Point,
        width: number,
        heigt: number,

    ) {
        this.iconImageUrl = iconImageUrl;
        this.target = target;
        this.description = description;
        this.point = point;
        this.width = width;
        this.height = heigt;
    }

    public getPoint(): Point {
        return this.point;
    }

    public getIconImageUrl(): string {
        return this.iconImageUrl;
    }

    public getTarget(): IMedium {
        return this.target;
    }

    public getWidth(): number {
        return this.width;
    }

    public getDescription(): string {
        return this.description;
    }
}
