import { Component, Input, OnChanges, OnInit } from '@angular/core';

import { CcMediaService } from '../utils/cc-media.service';
import { LogService } from '../utils/logging.service';
import { Icon } from './icon.model';

@Component({
    selector: '[svg-icon]',
    templateUrl: './ngx-gallery-svg-icon.component.html',
    styleUrls: ['./ngx-gallery-svg-icon.component.scss'],
})

export class NgxGallerySvgIconComponent implements OnInit, OnChanges {

    @Input() svgIcons: Icon[];
    @Input() scaleFactor = 1.0;

    widths: number[] = [];
    heights: number[] = [];

    constructor(private logService: LogService, private ccMediaService: CcMediaService) { }

    ngOnInit() {
        this.logService.log('NgxGallerySvgIconComponent.ngOnInit', 'scaleFactor - ' + this.scaleFactor);
        this.setWidths();
        this.setHeights();
    }

    ngOnChanges() {
        this.logService.log('NgxGallerySvgIconComponent.onChange', 'scaleFactor - ' + this.scaleFactor);
        this.setWidths();
        this.setHeights();
    }

    open(event: MouseEvent, icon: Icon) {
        this.ccMediaService.openMediumTarget(event, icon.target);
        this.logService.log('NgxGallerySvgIconComponent.click', 'icon target: ' + icon.target.id);
    }

    setWidths() {
        this.widths = this.svgIcons.map(icon => icon.width * this.scaleFactor);
    }

    setHeights() {
        this.heights = this.svgIcons.map(icon => icon.height * this.scaleFactor);
    }
}
