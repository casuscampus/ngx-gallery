import { Point } from './point.model';

export class Arrow  {
    startP: Point;
    endP: Point;

    constructor(
        startP: Point,
        endP: Point
    ) {
        this.startP = startP;
        this.endP = endP;
    }

    public getStartPoint(): Point {
        return this.startP;
    }

    public getDestPoint(): Point {
        return this.endP;
    }
}
