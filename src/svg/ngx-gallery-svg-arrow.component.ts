import { Component, OnInit, Input } from '@angular/core';
import { Arrow } from './arrow.model';
import { Point } from './point.model';
import { LogService } from '../utils/logging.service';

@Component({
    selector: '[svg-arrow]',
    templateUrl: './ngx-gallery-svg-arrow.component.html',
})

export class NgxGallerySvgArrowComponent implements OnInit {

    @Input() svgArrows: Arrow[];

    constructor(private logService: LogService) { }

    ngOnInit() {
        this.logService.log('NgxGallerySvgArrowComponent.ngOnInit', '...');
    }
}
