import { Point } from './point.model';
import { IMedium } from '@casus-campus/cc-support';

export class Polygon {
    text: string;
    points: Point[];
    target: IMedium;

    constructor(
        text: string,
        points: Point [],
        target: IMedium
    ) {
        this.text = text;
        this.points = points;
        this.target = target;
    }

    public getPoints(): Point [] {
        return this.points;
    }

    public getSVGPoints(): string {
        return [this.points[0], ...this.points]
            .map(point => point.getPosX() + ',' + point.getPosY())
            .join(' ');
    }

    public getSVGPointsPolyline(): string {
        return [this.points[0], ...this.points, this.points[0]]
            .map(point => point.getPosX() + ',' + point.getPosY())
            .join(' ');
    }

    public getText(): string {
        return this.text.trim();
    }
}
