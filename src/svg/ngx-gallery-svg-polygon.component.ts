import { Component, Input, OnChanges, OnInit } from '@angular/core';

import { CcMediaService } from '../utils/cc-media.service';
import { LogService } from '../utils/logging.service';
import { Polygon } from './polygon.model';

@Component({
    selector: '[svg-polygon]',
    templateUrl: './ngx-gallery-svg-polygon.component.html',
    styleUrls: ['./ngx-gallery-svg-polygon.component.scss'],
})

export class NgxGallerySvgPolygonComponent implements OnInit, OnChanges {

    @Input() svgPolygons: Polygon[];

    constructor(private logService: LogService, public ccMediaService: CcMediaService) { }

    ngOnInit() {
        this.logService.log('NgxGallerySvgPolygonComponent.ngOnInit', '...');
    }

    ngOnChanges() {
        this.logService.log('NgxGallerySvgPolygonComponent.ngOnChanges', '...');
    }

    open(event: MouseEvent, polygon: Polygon) {
        this.logService.log('NgxGallerySVGPolygonComponent.open', 'linkId: ' + polygon.target.id);
        this.ccMediaService.openMediumTarget(event, polygon.target);
    }
}
