import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Point } from './point.model';
import { LogService } from '../utils/logging.service';

@Component({
    selector: '[svg-text]',
    templateUrl: './ngx-gallery-svg-text.component.html',
    styleUrls: ['./ngx-gallery-svg-text.component.scss'],
})

export class NgxGallerySvgTextComponent implements OnInit, OnChanges {

    @Input() svgTexts: Point[];

    constructor(private logService: LogService) { }

    ngOnInit() {
        this.logService.log('NgxGallerySvgTextComponent.ngOnInit', 'count text: ' + this.svgTexts.length);
    }

    ngOnChanges() {
        this.logService.log('NgxGallerySvgTextComponent.onChange', 'count text: ' + this.svgTexts.length);
    }
}
