import { Component, OnInit, Input, OnChanges, ViewChild, ElementRef } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

import { LogService } from '../utils/logging.service';
import { NgxGalleryOrderedImage } from '../ngx-gallery-ordered-image.model';

@Component({
    selector: 'cc-video',
    templateUrl: './ngx-gallery-video.component.html',
    styleUrls: ['./ngx-gallery-video.component.scss'],
})

export class NgxGalleryVideoComponent implements OnInit, OnChanges {
    @ViewChild('mmelement') mmelement: ElementRef;

    @Input() image: NgxGalleryOrderedImage;
    @Input() selectedIndex: number;
    @Input() showDescription: boolean;
    @Input() description: string;
    @Input() mediaInfo: Object[];

    // constructor(private sanitization: DomSanitizer, private logService: LogService) {}
    constructor(private sanitization: DomSanitizer, private logService: LogService) {}

    ngOnInit() {
        this.logService.log('NgxGalleryVideoComponent.ngOnInit', 'starts ...');
    }

    ngOnChanges() {
        this.logService.log('NgxGalleryVideoComponent.onChange', 'starts ...');
    }

    getSafeVideoUrl(video: string): SafeUrl {
        return this.sanitization.bypassSecurityTrustResourceUrl(video);
    }

    timeUpdate() {
        this.logService.log('NgxGalleryImageComponent.timeUpdate', this.mmelement.nativeElement.currentTime);
    }
}
