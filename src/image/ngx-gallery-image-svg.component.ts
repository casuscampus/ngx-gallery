import {
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    ElementRef,
    Input,
    OnChanges,
    OnInit,
    SimpleChanges,
    ViewChild,
} from '@angular/core';
import {
    IMediumAddOn,
    IMediumAddOnArrow,
    IMediumAddOnHotSpot,
    IMediumAddOnIconHyperlink,
    IMediumAddOnText,
    MediumAddOnType,
} from '@casus-campus/cc-support';

import { Arrow } from '../svg/arrow.model';
import { Icon } from '../svg/icon.model';
import { Point } from '../svg/point.model';
import { Polygon } from '../svg/polygon.model';
import { CcMediaService } from '../utils/cc-media.service';
import { LogService } from '../utils/logging.service';

@Component({
    selector: 'cc-imagesvg',
    templateUrl: './ngx-gallery-image-svg.component.html',
    styleUrls: ['./ngx-gallery-image-svg.component.scss'],
})

export class NgxGalleryImageSvgComponent implements OnInit, OnChanges, AfterViewInit {

    @Input() width: number;
    @Input() height: number;
    @Input() viewBox: string;
    @Input() posLeft: string;
    @Input() posTop: string;
    @Input() zoomValue: string;
    @Input() animation: string;
    @Input() addOns: IMediumAddOn[];
    @Input() classGrab: boolean;
    @Input() styleTransform: string;
    @Input() preview: boolean;

    svgArrows: Arrow[] = [];
    svgPolygons: Polygon[] = [];
    svgIcons: Icon[] = [];
    svgTexts: Point[] = [];

    scaleFactor = 1;

    @ViewChild('svgElement') svgElement: ElementRef;

    constructor(private logService: LogService, private ccMediaService: CcMediaService, private cdRef: ChangeDetectorRef) { }

    ngOnInit() {
        this.logService.log('NgxGallerySvgComponent.ngOnInit', 'ngOnInit');
        this.handleAddOns();
    }

    ngAfterViewInit() {
        this.scaleFactor = this.calcScaleFactor();
        this.cdRef.detectChanges();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.logService.log('NgxGallerySvgComponent.onChange', 'ngOnChanges');
        if (changes.zoomValue && !changes.zoomValue.isFirstChange()) {
            this.scaleFactor = this.calcScaleFactor();
        }
    }

    private handleAddOns(): void {
        this.svgArrows = this.addOns
            .filter(addOn => addOn.type === MediumAddOnType.Arrow)
            .map(addOn => this.mapToArrow(addOn as IMediumAddOnArrow))
            .filter(element => !!element);
        this.svgIcons = this.addOns
            .filter(addOn => addOn.type === MediumAddOnType.IconHyperlink)
            .map(addOn => this.mapToIcon(addOn as IMediumAddOnIconHyperlink))
            .filter(element => !!element);
        this.svgPolygons = this.addOns
            .filter(addOn => addOn.type === MediumAddOnType.HotSpot)
            .map(addOn => this.mapToPolygon(addOn as IMediumAddOnHotSpot))
            .filter(element => !!element);
        this.svgTexts = this.addOns
            .filter(addOn => addOn.type === MediumAddOnType.Text)
            .map(addOn => this.mapToText(addOn as IMediumAddOnText))
            .filter(element => !!element);
    }

    private mapToArrow(arrow: IMediumAddOnArrow): Arrow {
        return new Arrow(
            new Point(arrow.src.left, arrow.src.top, ''),
            new Point(arrow.dst.left, arrow.dst.top, '')
        );
    }

    private mapToIcon(iconHyperlink: IMediumAddOnIconHyperlink): Icon {
        if (!iconHyperlink.medium) {
            console.error('Found iconHyperlink without target', iconHyperlink);
            return;
        }
        const placeholderPath = this.ccMediaService.getMimeTypePlaceholderPath(iconHyperlink.medium.mimeType);
        return new Icon(
            placeholderPath,
            iconHyperlink.medium,
            iconHyperlink.medium.description,
            new Point(iconHyperlink.rect.left, iconHyperlink.rect.top, ''),
            iconHyperlink.rect.width,
            iconHyperlink.rect.height
        );
    }

    private mapToPolygon(hotSpot: IMediumAddOnHotSpot): Polygon {
        const points = hotSpot.polygon
            .map(point => new Point(point.left, point.top, ''));
        return new Polygon(
            (hotSpot.medium && hotSpot.medium.description) || '',
            points,
            hotSpot.medium
        );

    }

    private mapToText(text: IMediumAddOnText): Point {
        return new Point(
            text.position.left,
            text.position.top,
            text.text
        );
    }

    // Returns multiplicator for elements that should keep the same dimensions
    private calcScaleFactor(): number {

        const originalWidth = parseFloat(this.viewBox.split(' ')[2]);
        const currentWidth = this.svgElement.nativeElement.clientWidth * parseFloat(this.zoomValue);
        const widthScaleFactor = originalWidth / currentWidth;

        const originalHeight = parseFloat(this.viewBox.split(' ')[3]);
        const currentHeigth = this.svgElement.nativeElement.clientHeight * parseFloat(this.zoomValue);
        const heightScaleFactor = originalHeight / currentHeigth;

        return Math.max(widthScaleFactor, heightScaleFactor);
    }
}
