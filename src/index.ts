import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';

import { NgxGalleryActionComponent } from './ngx-gallery-action.component';
import { NgxGalleryArrowsComponent } from './ngx-gallery-arrows.component';
import { NgxGalleryBulletsComponent } from './ngx-gallery-bullets.component';
import { NgxGalleryImageComponent } from './ngx-gallery-image.component';
import { NgxGalleryThumbnailsComponent } from './ngx-gallery-thumbnails.component';
import { NgxGalleryPreviewComponent } from './ngx-gallery-preview.component';
import { NgxGallerySvgTextComponent } from './svg/ngx-gallery-svg-text.component';
import { NgxGallerySvgArrowComponent } from './svg/ngx-gallery-svg-arrow.component';
import { NgxGallerySvgIconComponent } from './svg/ngx-gallery-svg-icon.component';
import { NgxGallerySvgPolygonComponent } from './svg/ngx-gallery-svg-polygon.component';
import { NgxGalleryImageSvgComponent } from './image/ngx-gallery-image-svg.component';
import { NgxGalleryVideoComponent } from './video/ngx-gallery-video.component';
import { NgxGalleryComponent } from './ngx-gallery.component';
import { UtilsScriptComponent } from './utils/utils-script.component';
import { PreviewLoaderComponent } from './utils/ngx-preview-loader.component'

import { LogService } from './utils/logging.service';

export * from './ngx-gallery.component';
export * from './ngx-gallery-action.component';
export * from './ngx-gallery-image.component';
export * from './ngx-gallery-thumbnails.component';
export * from './ngx-gallery-preview.component';
export * from './ngx-gallery-arrows.component';
export * from './ngx-gallery-bullets.component';
export * from './ngx-gallery-options.model';
export * from './ngx-gallery-image.model';
export * from './ngx-gallery-animation.model';
export * from './ngx-gallery-helper.service';
export * from './ngx-gallery-image-size.model';
export * from './ngx-gallery-layout.model';
export * from './ngx-gallery-order.model';
export * from './ngx-gallery-ordered-image.model';
export * from './ngx-gallery-action.model';
// export * from './utils/dom.service';
export * from './svg/ngx-gallery-svg-text.component';
export * from './svg/ngx-gallery-svg-arrow.component';
export * from './svg/ngx-gallery-svg-icon.component';
export * from './svg/ngx-gallery-svg-polygon.component';
export * from './image/ngx-gallery-image-svg.component';
export * from './video/ngx-gallery-video.component';
export * from './utils/utils-script.component';
export * from './utils/ngx-preview-loader.component';

export * from './cc-custom-options';
export * from './utils/cc-media.service';
export * from './utils/logging.service';

export class CustomHammerConfig extends HammerGestureConfig  {
    overrides = <any>{
        'pinch': { enable: false },
        'rotate': { enable: false }
    };
}

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [
        NgxGallerySvgPolygonComponent,
        NgxGallerySvgIconComponent,
        NgxGallerySvgArrowComponent,
        NgxGallerySvgTextComponent,
        NgxGalleryImageSvgComponent,
        NgxGalleryVideoComponent,
        NgxGalleryActionComponent,
        NgxGalleryArrowsComponent,
        NgxGalleryBulletsComponent,
        NgxGalleryImageComponent,
        NgxGalleryThumbnailsComponent,
        NgxGalleryPreviewComponent,
        NgxGalleryComponent,
        UtilsScriptComponent,
        PreviewLoaderComponent
    ],
    exports: [
        NgxGalleryComponent,
        PreviewLoaderComponent
    ],
    providers: [
        { provide: HAMMER_GESTURE_CONFIG, useClass: CustomHammerConfig },
        LogService,
    ]
})
export class NgxGalleryModule {}
