import { Injectable } from '@angular/core';
import { IMedium, IOpenMediumHyperlinkEvent } from '@casus-campus/cc-support';
import { BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';

import { NgxGalleryImage } from '../ngx-gallery-image.model';
import { LogService } from './logging.service';
import { MediaDimension } from './media-dimension.interface';
import { NgxGalleryDimensions } from './ngx-gallery-dimensions.interface';

@Injectable()
export class CcMediaService {

  private readonly _openedMediumTargets$ = new BehaviorSubject<IOpenMediumHyperlinkEvent>(null);
  readonly openedMediumTargets$ = this._openedMediumTargets$.asObservable().pipe(filter(openedMedium => !!openedMedium));

  constructor(private logService: LogService) { }

  prepareMedia(media: NgxGalleryImage[], dimensions: NgxGalleryDimensions): NgxGalleryImage[] {
    return media.map(medium => this.addUrlsAndDescriptionToMedium(medium, dimensions));
  }

  containsVideoOrAudio(mimeTypes: string[]): boolean {
    const hasVideo = !!mimeTypes.find(mimeType => this.isVideo(mimeType) || this.isAudio(mimeType));
    return hasVideo;
  }

  isExternalVideo(mimeType: string): boolean {
    return 'video/exthtml5' === mimeType;
  }

  isPDF(mimeType: string): boolean {
    return this.getMultimediaType(mimeType) === 'pdf';
  }

  isVideo(mimeType: string): boolean {
    return this.getMultimediaType(mimeType) === 'video';
  }

  isAudio(mimeType: string): boolean {
    return this.getMultimediaType(mimeType) === 'audio';
  }

  isImage(mimeType: string): boolean {
    return this.getMultimediaType(mimeType) === 'image';
  }

  getMultimediaType(mimeType: string): string {
    if (!mimeType) {
      return 'image';
    }

    if (mimeType.startsWith('audio')) {
      return 'audio';
    }

    if (mimeType.startsWith('video')) {
      return 'video';
    }

    if (mimeType === 'application/pdf') {
      return 'pdf';
    }

    if (mimeType.indexOf('application/campus') >= 0) {
      return 'campus';
    }

    return 'image';
  }

  openMediumTarget(event: MouseEvent, medium: IMedium) {
    if (!medium) {
      return;
    }
    this._openedMediumTargets$.next({sourceEvent: event, medium: medium});
    this.logService.log('CcMediaService.openMediumLink', 'change state: ' + medium.id)
  }

  private addUrlsAndDescriptionToMedium(medium: NgxGalleryImage, dimensions: NgxGalleryDimensions) {
    return {
      small: this.getSmallImageUrl(medium, dimensions.thumbnail),
      medium: this.getMediumImageUrl(medium, dimensions.image),
      big: this.getBigImageUrl(medium),
      // TODO: for what do we need the poster url?
      poster: this.getPosterUrl(medium),
      ccpshortdescription: medium.description && medium.description.substr(0, 72),
      suffix: this.getSuffix(medium),
      // TODO: why only for pdf? do we need this anyway?
      url: medium.type === 'application/pdf' && this.getImageUrl(medium),
      ...medium,
    };
  }

  // image urls - getter methods

  private getSmallImageUrl(medium: NgxGalleryImage, dimension: MediaDimension): string {
    // console.log('NgxGalleryConfig.getSmallImageUrl - starts');
    let url: string;
    if (this.isImage(medium.type)) {
      url = this.getImageUrl(medium, dimension.width, dimension.height);
    } else {
      url = this.getMimeTypePlaceholderPath(medium.type);
    }
    return url;
  }

  private getMediumImageUrl(medium: NgxGalleryImage, dimension: MediaDimension): string {
    // console.log('NgxGalleryConfig.getMediumImageUrl - starts');
    let url: string;
    if (this.isImage(medium.type)) {
      url = this.getImageUrl(medium, dimension.width, dimension.height);
    } else if (this.isPDF(medium.type)) {
      url = this.getMimeTypePlaceholderPath(medium.type);
    } else {
      url = this.getGenericUrl(medium);
    }
    return url;
  }

  private getBigImageUrl(medium: NgxGalleryImage): string {
    // console.log('NgxGalleryConfig.getBigImageUrl - starts');
    let url: string;
    if (this.isImage(medium.type)) {
      url = this.getImageUrl(medium, medium.width, medium.height);
    } else if (this.isPDF(medium.type)) {
      url = this.getMimeTypePlaceholderPath(medium.type);
    } else {
      url = this.getGenericUrl(medium);
    }
    return url;
  }

  private getPosterUrl(medium: NgxGalleryImage): string {
    let url: string;

    if (this.isAudio(medium.type) || this.isVideo(medium.type)) {
      url = this.getMimeTypePlaceholderPath(medium.type);
      // } else {
      //    console.log('NgxGalleryConfig.getPosterUrl - should never be called: ' + medium.type);
    }
    return url;
  }

  private getSuffix(medium: NgxGalleryImage): string {
    const url = this.getImageUrl(medium);
    if (!url) {
      return;
    }
    return url.substring(url.lastIndexOf('.') + 1)
  }

  private getGenericUrl(medium: NgxGalleryImage): string {
    switch (medium.type) {
      case 'application/campus-html5-puzzle':
      case 'application/campus-html5-mouseover':
        return this.getCampusUrl(medium);

      default:
        return this.getImageUrl(medium);
    }
  }

  private getImageUrl(medium: NgxGalleryImage, width?: number, height?: number): string {
    if (!medium.urls || !medium.urls.length) {
      return;
    }

    const url = medium.urls[0].url;
    if (!width || !height) {
      return url;
    }

    let adjustedWidth = width;
    let adjustedHeight = height;

    if (medium.width >= medium.height) {
      adjustedWidth = Math.min(adjustedWidth, medium.width);
    } else {
      adjustedHeight = Math.min(adjustedHeight, medium.height);
    }
    // console.log('NGXGalleryConfig.getImageUrl: medium.name - ' + medium.name + ' / width x height:  ' + width + 'x' + height);
    // console.log('NGXGalleryConfig.getImageUrl: url - ' + getImageUrl(url, adjustedWidth, adjustedHeight));
    return this.getAdjustedImageUrl(url, adjustedWidth, adjustedHeight);
  }

  private getAdjustedImageUrl(imageUrl: string, width: number, height: number) {
    let splitted: string[];
    splitted = imageUrl.split('.');
    return splitted.slice(0, -1).join('.') + '_' + width + 'x' + height + '.' + splitted.pop();
  }

  private getCampusUrl(medium: NgxGalleryImage): string {
    if (!medium.urls || !medium.urls.length) {
      return;
    }

    const url = medium.urls[0].url;
    return url + '.d/index.html';
  }

  // url used as placeholder image for pdf, audio and video
  // TODO: rename to getMimeTypePlaceholderUrl
  getMimeTypePlaceholderPath(mimeType: string): string {
    // TODO: don't use paths here, or add remark to README
    if (this.isAudio(mimeType)) {
      // free alternative: https://www.iconfinder.com/icons/2639771/audio_icon
      return 'https://cdn1.iconfinder.com/data/icons/ios-11-glyphs/30/audio-512.png';
      // return 'assets/audio-icon.png';
    }

    if (this.isVideo(mimeType)) {
      // free alternative: https://www.iconfinder.com/icons/3790609/audio_media_multimedia_icon
      return 'https://cdn3.iconfinder.com/data/icons/media-62/512/multimedia_audio_media-24-512.png';
      return 'assets/video-icon.jpg';
    }

    if (this.isPDF(mimeType)) {
      // free alternative: https://www.iconfinder.com/icons/2133056/document_eps_file_format_pdf_icon
      // return 'assets/pdf_document.png';
      return 'assets/pdf-icon.jpg';
    }

    return 'https://cdn3.iconfinder.com/data/icons/media-62/512/multimedia_audio_media-24-512.png';
  }

}
