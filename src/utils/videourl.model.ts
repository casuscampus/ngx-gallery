export class VideoUrl {
    public type: MediumUrlType;
    public url: string;
    public mimeType: string;

    constructor(type: MediumUrlType, url: string, mimeType: string) {
        this.type = type;
        this.url = url;
        this.mimeType = mimeType;
    }
}

export enum MediumUrlType {
    Internal,
    External
}
