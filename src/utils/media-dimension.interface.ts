export interface MediaDimension {
  width: number;
  height: number;
}
