import { SafeResourceUrl } from '@angular/platform-browser';
import { NgxGalleryPreviewComponent } from '../ngx-gallery-preview.component';
import { NgxGalleryOrderedImage } from '../ngx-gallery-ordered-image.model';
import { NgxGalleryAction } from '../ngx-gallery-action.model';
import { Point } from '../svg/point.model';
import { Arrow } from '../svg/arrow.model';
import { Icon } from '../svg/icon.model';
import { Polygon } from '../svg/polygon.model';

import {
    Component,
    EventEmitter,
    Output,
    OnChanges,
    Input,
    Injector,
    ComponentFactoryResolver,
    EmbeddedViewRef,
    ApplicationRef,
    SimpleChanges,
} from '@angular/core';

@Component({
    selector: 'ngx-gallery-preview-loader',
    template: ``,
    styleUrls: ['./ngx-preview-loader.component.scss'],
})

export class PreviewLoaderComponent implements OnChanges {
    private componentRef: any;
    preview: NgxGalleryPreviewComponent;

    @Input() onlyPreview: boolean;
    @Input() previewEnabled = false;
    @Input() prevIndex: number;
    @Input() images: string[] | SafeResourceUrl[];
    @Input() ordImages: NgxGalleryOrderedImage[];
    @Input() mediaInfos: Object[];
    @Input() links: string[];
    @Input() descriptions: string[];
    @Input() types: string[];
    @Input() mtypes: string[];
    @Input() widths: number[];
    @Input() heights: number[];
    @Input() svgArrows: Arrow[][];
    @Input() svgIcons: Icon[][];
    @Input() svgPolygons: Polygon[][];
    @Input() svgTexts: Point[][];
    @Input() showDescription: boolean;
    @Input() swipe: boolean;
    @Input() fullscreen: boolean;
    @Input() forceFullscreen: boolean;
    @Input() closeOnClick: boolean;
    @Input() closeOnEsc: boolean;
    @Input() keyboardNavigation: boolean;
    @Input() arrowPrevIcon: string;
    @Input() arrowNextIcon: string;
    @Input() closeIcon: string;
    @Input() fullscreenIcon: string;
    @Input() spinnerIcon: string;
    @Input() autoPlay: boolean;
    @Input() autoPlayInterval: number;
    @Input() autoPlayPauseOnHover: boolean;
    @Input() infinityMove: boolean;
    @Input() interactive: boolean;
    @Input() hasSVGArrow: boolean;
    @Input() hasSVGPolygon: boolean;
    @Input() hasSVGIcon: boolean;
    @Input() hasSVGText: boolean;
    @Input() isSVGActive: boolean;
    @Input() zoom: boolean;
    @Input() zoomStep: number;
    @Input() zoomMax: number;
    @Input() zoomMin: number;
    @Input() interactiveIcon: string;
    @Input() zoomInIcon: string;
    @Input() zoomOutIcon: string;
    @Input() animation: boolean;
    @Input() actions: NgxGalleryAction[];
    @Input() rotate: boolean;
    @Input() rotateLeftIcon: string;
    @Input() rotateRightIcon: string;
    @Input() download: boolean;
    @Input() downloadIcon: string;
    @Input() bullets: string;
    @Input() name: string;
    @Input() openIndex = -1;
    @Input() isActive: boolean;

    @Output() onOpen = new EventEmitter();
    @Output() onClose = new EventEmitter();
    @Output() onActiveChange = new EventEmitter<number>();

    constructor(private componentFactoryResolver: ComponentFactoryResolver,
        private appRef: ApplicationRef,
        private injector: Injector) {
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (this.componentRef) {
            this.componentRef.instance.onlyPreview = this.onlyPreview;
            this.componentRef.instance.previewEnabled = this.previewEnabled;
            this.componentRef.instance.prevIndex = this.prevIndex;
            this.componentRef.instance.images = this.images;
            this.componentRef.instance.ordImages = this.ordImages;
            this.componentRef.instance.mediaInfos = this.mediaInfos;
            this.componentRef.instance.links = this.links;
            this.componentRef.instance.descriptions = this.descriptions;
            this.componentRef.instance.types = this.types;
            this.componentRef.instance.mtypes = this.mtypes;
            this.componentRef.instance.widths = this.widths;
            this.componentRef.instance.heights = this.heights;
            this.componentRef.instance.svgArrows = this.svgArrows;
            this.componentRef.instance.svgIcons = this.svgIcons;
            this.componentRef.instance.svgPolygons = this.svgPolygons;
            this.componentRef.instance.svgTexts = this.svgTexts;
            this.componentRef.instance.showDescription = this.showDescription;
            this.componentRef.instance.swipe = this.swipe;
            this.componentRef.instance.fullscreen = this.fullscreen;
            this.componentRef.instance.forceFullscreen = this.forceFullscreen;
            this.componentRef.instance.closeOnClick = this.closeOnClick;
            this.componentRef.instance.closeOnEsc = this.closeOnEsc;
            this.componentRef.instance.keyboardNavigation = this.keyboardNavigation;
            this.componentRef.instance.arrowPrevIcon = this.arrowPrevIcon;
            this.componentRef.instance.arrowNextIcon = this.arrowNextIcon;
            this.componentRef.instance.closeIcon = this.closeIcon;
            this.componentRef.instance.fullscreenIcon = this.fullscreenIcon;
            this.componentRef.instance.spinnerIcon = this.spinnerIcon;
            this.componentRef.instance.autoPlay = this.autoPlay;
            this.componentRef.instance.autoPlayInterval = this.autoPlayInterval;
            this.componentRef.instance.autoPlayPauseOnHover = this.autoPlayPauseOnHover;
            this.componentRef.instance.infinityMove = this.infinityMove;
            this.componentRef.instance.interactive = this.interactive;
            this.componentRef.instance.hasSVGArrow = this.hasSVGArrow;
            this.componentRef.instance.hasSVGPolygon = this.hasSVGPolygon;
            this.componentRef.instance.hasSVGIcon = this.hasSVGIcon;
            this.componentRef.instance.hasSVGText = this.hasSVGText;
            this.componentRef.instance.isSVGActive = this.isSVGActive;
            this.componentRef.instance.zoom = this.zoom;
            this.componentRef.instance.zoomStep = this.zoomStep;
            this.componentRef.instance.zoomMax = this.zoomMax;
            this.componentRef.instance.zoomMin = this.zoomMin;
            this.componentRef.instance.interactiveIcon = this.interactiveIcon;
            this.componentRef.instance.zoomInIcon = this.zoomInIcon;
            this.componentRef.instance.zoomOutIcon = this.zoomOutIcon;
            this.componentRef.instance.animation = this.animation;
            this.componentRef.instance.actions = this.actions;
            this.componentRef.instance.rotate = this.rotate;
            this.componentRef.instance.rotateLeftIcon = this.rotateLeftIcon;
            this.componentRef.instance.rotateRightIcon = this.rotateRightIcon;
            this.componentRef.instance.download = this.download;
            this.componentRef.instance.downloadIcon = this.downloadIcon;
            this.componentRef.instance.bullets = this.bullets;
            this.componentRef.instance.name = this.name;
            this.componentRef.instance.openIndex = this.openIndex;
            this.componentRef.instance.isActive = this.isActive;
            this.componentRef.instance.onOpen.subscribe((event) => {
                this.onOpen.emit(event);
            })
            this.componentRef.instance.onClose.subscribe((event) => {
                this.onClose.emit(event);
                this.appRef.detachView(this.componentRef.hostView);
                this.componentRef.destroy();
            })
            this.componentRef.instance.onActiveChange.subscribe((event) => {
                this.onActiveChange.emit(event);
            })
            if (this.isActive) {
                this.preview = (<NgxGalleryPreviewComponent>this.componentRef.instance);
                this.preview.open(this.prevIndex);
                this.preview.ngOnChanges(changes);
            }
        }
    }

    public loadPreview() {
        const factory = this.componentFactoryResolver.resolveComponentFactory(NgxGalleryPreviewComponent);
        this.componentRef = factory.create(this.injector);
        this.appRef.attachView(this.componentRef.hostView);
        const domElem = (this.componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
        const cdkOverlayContainers = document.getElementsByClassName('cdk-overlay-container');
        let parentElement: Element;
        if (cdkOverlayContainers && cdkOverlayContainers.length) {
            parentElement = cdkOverlayContainers.item(0);
        } else {
            parentElement = document.body;
        }
        parentElement.appendChild(domElem);
    }
}
