import { MediaDimension } from './media-dimension.interface';

export interface NgxGalleryDimensions {
  box: MediaDimension;
  image: MediaDimension;
  thumbnail: MediaDimension;
}
