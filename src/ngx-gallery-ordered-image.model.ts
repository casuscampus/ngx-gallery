import { SafeResourceUrl } from '@angular/platform-browser';
import { VideoUrl } from './utils/videourl.model';
import { IMediumAddOn } from '@casus-campus/cc-support';

export interface INgxGalleryOrderedImage {
    id?: string;
    src: string | SafeResourceUrl;
    autoplay?: boolean;
    loop?: boolean;
    poster?: string | SafeResourceUrl;
    urls?: VideoUrl[];
    index: number;
    addOns?: IMediumAddOn[];
}

export class NgxGalleryOrderedImage implements INgxGalleryOrderedImage {
    id?: string;
    src: string | SafeResourceUrl;
    autoplay?: boolean;
    loop?: boolean;
    poster?: string | SafeResourceUrl;
    urls?: VideoUrl[];
    index: number;
    addOns?: IMediumAddOn[];

    constructor(obj: INgxGalleryOrderedImage) {
        this.id = obj.id;
        this.src = obj.src;
        this.autoplay =  obj.autoplay || false;
        this.loop = obj.loop || false;
        this.poster = obj.poster;
        this.urls = obj.urls || [];
        this.index = obj.index;
        this.addOns = obj.addOns || [];
    }
}
