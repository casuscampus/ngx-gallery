import { NgxGalleryImage } from './ngx-gallery-image.model';
import { } from 'jasmine';
import { Renderer, SimpleChange } from '@angular/core';
import { TestBed, ComponentFixture, async } from '@angular/core/testing';
import {
    NgxGalleryComponent, NgxGalleryActionComponent, NgxGalleryImageComponent, NgxGalleryThumbnailsComponent,
    NgxGalleryPreviewComponent, NgxGalleryArrowsComponent, NgxGalleryBulletsComponent, NgxGalleryHelperService,
    NgxGalleryOptions
} from './';
import { CcMediaService } from './utils/cc-media.service';
import { LogService } from './utils/logging.service';
import { NgxGallerySvgArrowComponent } from './svg/ngx-gallery-svg-arrow.component';
import { NgxGallerySvgIconComponent } from './svg/ngx-gallery-svg-icon.component';
import { NgxGallerySvgPolygonComponent } from './svg/ngx-gallery-svg-polygon.component';
import { NgxGallerySvgTextComponent } from './svg/ngx-gallery-svg-text.component';
import { NgxGalleryImageSvgComponent } from './image/ngx-gallery-image-svg.component';
import { NgxGalleryVideoComponent } from './video/ngx-gallery-video.component';
import { UtilsScriptComponent } from './utils/utils-script.component';


describe('NgxGalleryComponent', () => {
    let fixture: ComponentFixture<NgxGalleryComponent>;
    let comp: NgxGalleryComponent;
    let el, prevArrow, nextArrow, image;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                NgxGalleryComponent,
                NgxGalleryActionComponent,
                NgxGalleryThumbnailsComponent,
                NgxGalleryImageComponent,
                NgxGalleryPreviewComponent,
                NgxGalleryArrowsComponent,
                NgxGalleryBulletsComponent,
                NgxGallerySvgArrowComponent,
                NgxGallerySvgIconComponent,
                NgxGallerySvgPolygonComponent,
                NgxGallerySvgTextComponent,
                NgxGalleryImageSvgComponent,
                NgxGalleryVideoComponent,
                UtilsScriptComponent,
            ],
            providers: [
                NgxGalleryHelperService,
                Renderer,
                CcMediaService,
                { provide: LogService, useValue: {log: (fName: string, msg: string, prioLevel?: number) => null} },
             ]
        })
            .overrideComponent(NgxGalleryComponent, {
                set: {
                    styleUrls: [],
                }
            })
            .compileComponents()
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NgxGalleryComponent);
        comp = fixture.componentInstance;

        comp.images = [
            new NgxGalleryImage({
                small: 'https://picsum.photos/100',
                medium: 'https://picsum.photos/200',
                big: 'https://picsum.photos/300',
                type: 'image/jpg',
                width: 100,
                height: 100,
                urls: [
                    {type: 0, url: 'https://picsum.photos/400', mimeType: 'image/jpg'},
                ]
            }),
            new NgxGalleryImage({
                small: 'https://picsum.photos/101',
                medium: 'https://picsum.photos/201',
                big: 'https://picsum.photos/301',
                type: 'image/jpg',
                width: 100,
                height: 100,
                urls: [
                    {type: 0, url: 'https://picsum.photos/401', mimeType: 'image/jpg'},
                ]
            }),
            new NgxGalleryImage({
                small: 'https://picsum.photos/102',
                medium: 'https://picsum.photos/202',
                big: 'https://picsum.photos/302',
                type: 'image/jpg',
                width: 100,
                height: 100,
                urls: [
                    {type: 0, url: 'https://picsum.photos/402', mimeType: 'image/jpg'},
                ]
            })
        ];
        comp.options = [ new NgxGalleryOptions({}) ];
        fixture.detectChanges();

        el = fixture.debugElement.nativeElement;
    });

    it('should create component', () => {
        expect(fixture.componentInstance).toBeTruthy();
    });

    it('should return true from canShowNext if there are more images', () => {
        comp.selectedIndex = 0;
        expect(comp.canShowNext()).toBeTruthy();
    });

    it('should return false from canShowNext if there are no images', () => {
        comp.images = undefined;
        expect(comp.canShowNext()).toBeFalsy();
    });

    it('should return true from canShowPrev if there are more images', () => {
        comp.selectedIndex = 1;
        expect(comp.canShowPrev()).toBeTruthy();
    });

    it('should return false from canShowPrev if there are no images', () => {
        comp.images = undefined;
        expect(comp.canShowPrev()).toBeFalsy();
    });
})
