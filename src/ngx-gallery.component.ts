import {
    Component, Input, ViewChild, OnInit, HostBinding, ElementRef,
    AfterViewInit, Output, EventEmitter, OnChanges, SimpleChanges, OnDestroy
} from '@angular/core';
import { SafeResourceUrl } from '@angular/platform-browser';

import { NgxGalleryPreviewComponent } from './ngx-gallery-preview.component';
import { NgxGalleryImageComponent } from './ngx-gallery-image.component';
import { NgxGalleryThumbnailsComponent } from './ngx-gallery-thumbnails.component';
import { NgxGalleryHelperService } from './ngx-gallery-helper.service';

import { NgxGalleryOptions } from './ngx-gallery-options.model';
import { NgxGalleryImage } from './ngx-gallery-image.model';
import { NgxGalleryLayout } from './ngx-gallery-layout.model';
import { NgxGalleryOrderedImage } from './ngx-gallery-ordered-image.model';

import { NgxGalleryAction } from './ngx-gallery-action.model';

import { LogService } from './utils/logging.service';
import { CcCustomOptions } from './cc-custom-options';
import { CcMediaService } from './utils/cc-media.service';

import { Md5 } from 'ts-md5/dist/md5';
import { Subscription } from 'rxjs';
import { PreviewLoaderComponent } from './utils/ngx-preview-loader.component';
import { IOpenMediumHyperlinkEvent } from '@casus-campus/cc-support';

@Component({
    selector: 'ngx-gallery',
    template: `
    <div class="ngx-gallery-layout {{currentOptions?.layout}}" [ngStyle]="styleBox">
        <!--
            currently resizing window does NOT lead to a resize event, if external video in an iframe is shown
            problem: maximizing and minimizing the video would not be possible!
        -->
        <ng-container *ngIf="ccMediaService.isExternalVideo(types[selectedIndex]); else onResizeTmpl">
        </ng-container>

        <ng-template #onResizeTmpl>
            <div (window:resize)="onResize($event)"></div>
        </ng-template>

        <ngx-gallery-image *ngIf="currentOptions?.image"
            [name]="name"
            [style.height]="getImageHeight()"
            [images]="mediumImages"
            [imageWidth]="imageWidth"
            [imageHeight]="imageHeight"
            [imageVBox]="imageVBox"
            [hasVideo]="hasVideo"
            [positionX]="positionX"
            [positionY]="positionY"
            [links]="currentOptions?.ccpImageAsLink ? links : []"
            [widths]="widths"
            [heights]="heights"
            [mediaInfos]="mediaInfos"
            [types]="types"
            [mtypes]="mtypes"
            [isSVGActive]="isSVGActive"
            [clickable]="currentOptions?.preview"
            [selectedIndex]="selectedIndex"
            [arrows]="currentOptions?.imageArrows"
            [arrowsAutoHide]="currentOptions?.imageArrowsAutoHide"
            [arrowPrevIcon]="currentOptions?.arrowPrevIcon"
            [arrowNextIcon]="currentOptions?.arrowNextIcon"
            [swipe]="currentOptions?.imageSwipe"
            [animation]="currentOptions?.imageAnimation"
            [size]="currentOptions?.imageSize"
            [autoPlay]="currentOptions?.imageAutoPlay"
            [autoPlayInterval]="currentOptions?.imageAutoPlayInterval"
            [autoPlayPauseOnHover]="currentOptions?.imageAutoPlayPauseOnHover"
            [infinityMove]="currentOptions?.imageInfinityMove"
            [lazyLoading]="currentOptions?.lazyLoading"
            [actions]="currentOptions?.imageActions"
            [descriptions]="descriptions"
            [showDescription]="currentOptions?.imageDescription"
            [bullets]="currentOptions?.imageBullets"
            (onClick)="openPreview($event)"
            (onActiveChange)="selectFromImage($event)">
        </ngx-gallery-image>

        <ngx-gallery-thumbnails
            *ngIf="currentOptions?.thumbnails" [style.marginTop]="getThumbnailsMarginTop()"
            [name]="name"
            [style.marginBottom]="getThumbnailsMarginBottom()"
            [style.height]="getThumbnailsHeight()"
            [images]="smallImages"
            [links]="currentOptions?.thumbnailsAsLinks ? links : []"
            [labels]="labels" [linkTarget]="currentOptions?.linkTarget"
            [widths]="widths"
            [heights]="heights"
            [types]="types"
            [selectedIndex]="selectedIndex"
            [columns]="currentOptions?.thumbnailsColumns"
            [rows]="currentOptions?.thumbnailsRows"
            [margin]="currentOptions?.thumbnailMargin"
            [arrows]="currentOptions?.thumbnailsArrows"
            [arrowsAutoHide]="currentOptions?.thumbnailsArrowsAutoHide"
            [arrowPrevIcon]="currentOptions?.arrowPrevIcon"
            [arrowNextIcon]="currentOptions?.arrowNextIcon"
            [clickable]="currentOptions?.image || currentOptions?.preview"
            [swipe]="currentOptions?.thumbnailsSwipe"
            [size]="currentOptions?.thumbnailSize"
            [moveSize]="currentOptions?.thumbnailsMoveSize"
            [order]="currentOptions?.thumbnailsOrder"
            [remainingCount]="currentOptions?.thumbnailsRemainingCount"
            [lazyLoading]="currentOptions?.lazyLoading"
            [actions]="currentOptions?.thumbnailActions"
            (onActiveChange)="selectFromThumbnails($event)">
        </ngx-gallery-thumbnails>

        <ngx-gallery-preview-loader
            #previewLoader
            [name]="name"
            [prevIndex]="prevIndex"
            [images]="bigImages"
            [ordImages]="mediumImages"
            [mediaInfos]="mediaInfos"
            [links]="currentOptions?.ccpPreviewImageAsLink ? links : []"
            [descriptions]="descriptions"
            [widths]="widths"
            [heights]="heights"
            [types]="types"
            [mtypes]="mtypes"
            [showDescription]="currentOptions?.previewDescription"
            [arrowPrevIcon]="currentOptions?.arrowPrevIcon"
            [arrowNextIcon]="currentOptions?.arrowNextIcon"
            [closeIcon]="currentOptions?.closeIcon"
            [fullscreenIcon]="currentOptions?.fullscreenIcon"
            [spinnerIcon]="currentOptions?.spinnerIcon"
            [swipe]="currentOptions?.previewSwipe"
            [fullscreen]="currentOptions?.previewFullscreen"
            [forceFullscreen]="currentOptions?.previewForceFullscreen"
            [closeOnClick]="currentOptions?.previewCloseOnClick"
            [closeOnEsc]="currentOptions?.previewCloseOnEsc"
            [keyboardNavigation]="currentOptions?.previewKeyboardNavigation"
            [animation]="currentOptions?.previewAnimation"
            [autoPlay]="currentOptions?.previewAutoPlay"
            [autoPlayInterval]="currentOptions?.previewAutoPlayInterval"
            [autoPlayPauseOnHover]="currentOptions?.previewAutoPlayPauseOnHover"
            [infinityMove]="currentOptions?.previewInfinityMove"
            [isSVGActive]="isSVGActive"
            [zoom]="currentOptions?.previewZoom" [zoomStep]="currentOptions?.previewZoomStep"
            [zoomMax]="currentOptions?.previewZoomMax" [zoomMin]="currentOptions?.previewZoomMin"
            [zoomInIcon]="currentOptions?.zoomInIcon" [zoomOutIcon]="currentOptions?.zoomOutIcon"
            [actions]="currentOptions?.actions"
            [rotate]="currentOptions?.previewRotate"
            [rotateLeftIcon]="currentOptions?.rotateLeftIcon"
            [rotateRightIcon]="currentOptions?.rotateRightIcon"
            [download]="currentOptions?.previewDownload"
            [downloadIcon]="currentOptions?.downloadIcon"
            [bullets]="currentOptions?.previewBullets"
            (onClose)="onPreviewClose()" (onOpen)="onPreviewOpen()"
            (onActiveChange)="previewSelect($event)"
            [isActive]="previewEnabled"
        ></ngx-gallery-preview-loader>
    </div>`,
    styleUrls: ['./ngx-gallery.component.scss'],
    providers: [NgxGalleryHelperService, CcMediaService],
    entryComponents: [NgxGalleryPreviewComponent]
})

export class NgxGalleryComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {
    @Input() options: NgxGalleryOptions[];
    @Input() images: NgxGalleryImage[];
    @Input() name: string;

    @Output() imagesReady = new EventEmitter();
    @Output() change = new EventEmitter<{ index: number; image: NgxGalleryImage; }>();
    @Output() previewOpen = new EventEmitter();
    @Output() previewClose = new EventEmitter();
    @Output() previewChange = new EventEmitter<{ index: number; image: NgxGalleryImage; }>();
    @Output() mediumTargetOpen = new EventEmitter<IOpenMediumHyperlinkEvent>();

    styleBox: {};
    prevIndex: number;
    smallImages: string[] | SafeResourceUrl[];
    mediumImages: NgxGalleryOrderedImage[];
    bigImages: string[] | SafeResourceUrl[];
    descriptions: string[];
    hasVideo: boolean;
    links: string[];
    labels: string[];
    types: string[] = [];
    mtypes: string[];
    widths: number[];
    heights: number[];
    imageWidth: number[];
    imageHeight: number[];
    imageVBox: string[];
    positionX: number[];
    positionY: number[];
    mediaInfos: Object[];

    linkIds = {
        'image': ['1108408', '1108413', '1108410', '1108411', '994043', '1120513'], // image
        'audio': ['449544', '69378', '312031'], // audio
        'video': ['70741', '670180', '952730', '247647', '1073360', '1073366', '987542', '1018554'], // video
        'campus': ['1460438', '1460419'] // campus
    };
    customOptions: CcCustomOptions;

    selectedIndex = 0;

    pdfAction: NgxGalleryAction;
    prevAction: NgxGalleryAction;
    svgAction: NgxGalleryAction;

    isPDF = false;
    // TODO: this should be an option of an image
    isSVG = false;
    // TODO: rename to overlayVisible or similar
    isSVGActive = false;
    previewEnabled: boolean;

    currentOptions: NgxGalleryOptions;

    private breakpoint: number | undefined = undefined;
    private prevBreakpoint: number | undefined = undefined;
    private fullWidthTimeout: any;
    private subscriptions: Subscription[] = [];

    @ViewChild(PreviewLoaderComponent) previewLoader: PreviewLoaderComponent;
    @ViewChild(NgxGalleryImageComponent) image: NgxGalleryImageComponent;
    @ViewChild(NgxGalleryThumbnailsComponent) thubmnails: NgxGalleryThumbnailsComponent;

    @HostBinding('style.width') width: string;
    @HostBinding('style.height') height: string;
    @HostBinding('style.left') left: string;

    constructor(private myElement: ElementRef, private logService: LogService, public ccMediaService: CcMediaService) { }

    ngOnInit() {
        this.subscriptions.push(this.ccMediaService.openedMediumTargets$
            .subscribe(openedMedium => this.mediumTargetOpen.emit(openedMedium))
        );
        this.customOptions = new CcCustomOptions(this.options, this.images.length);
        this.initActions();
        this.initOptions();
        this.prepareSvgAndPdf();
        this.initImages();

        this.logService.log('NgxGalleryComponent.ngOnInit(' + this.name + ')', 'ccpBoxLayout - ' + this.currentOptions.ccpBoxLayout)
        this.logService.log('NgxGalleryComponent.ngOnInit(' + this.name + ')', 'selectedIndex - '
            + this.selectedIndex + ' / isPDF: ' + this.isPDF);
    }

    ngOnChanges(changes: SimpleChanges): void {
        const optionsChanged = !!changes.options && !changes.options.firstChange && !!changes.options.currentValue;
        const imagesChanged = !!changes.images && !changes.images.firstChange && !!changes.images.currentValue;

        if (optionsChanged) {
            this.logService.log('NgxGalleryComponent.ngOnChanges(' + this.name + ')', 'optionsChanges ...');
            this.customOptions.setOriginalOptions(changes.options.currentValue);
        }

        if (imagesChanged) {
            this.logService.log('NgxGalleryComponent.ngOnChanges(' + this.name + ')', 'imagesChanged ...');
            this.customOptions.setNumberOfMedia(changes.images.currentValue.length);
        }

        if (optionsChanged || imagesChanged) {
            this.initOptions();
            this.prepareSvgAndPdf();
            this.initImages();
        }
    }

    ngAfterViewInit(): void {
        this.checkFullWidth();
    }

    ngOnDestroy() {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

    // has to be modified due to external images (magnify) in iframe
    // @HostListener('window:resize', [ '$event' ]) onResize( event ) {
    onResize(): void {
        this.setBreakpoint();

        if (this.prevBreakpoint !== this.breakpoint) {
            this.setOptions();
            this.resetThumbnails();
        }

        if (this.currentOptions && this.currentOptions.fullWidth) {

            if (this.fullWidthTimeout) {
                clearTimeout(this.fullWidthTimeout);
            }

            this.fullWidthTimeout = setTimeout(() => {
                this.checkFullWidth();
            }, 200);
        }
    }

    getImageHeight(): string {
        return (this.currentOptions && this.currentOptions.thumbnails) ?
            this.currentOptions.imagePercent + '%' : '100%';
    }

    getThumbnailsHeight(): string {
        if (this.currentOptions && this.currentOptions.image) {
            return 'calc(' + this.currentOptions.thumbnailsPercent + '% - '
                + this.currentOptions.thumbnailsMargin + 'px)';
        } else {
            return '100%';
        }
    }

    getThumbnailsMarginTop(): string {
        if (this.currentOptions && this.currentOptions.layout === NgxGalleryLayout.ThumbnailsBottom) {
            return this.currentOptions.thumbnailsMargin + 'px';
        } else {
            return '0px';
        }
    }

    getThumbnailsMarginBottom(): string {
        if (this.currentOptions && this.currentOptions.layout === NgxGalleryLayout.ThumbnailsTop) {
            return this.currentOptions.thumbnailsMargin + 'px';
        } else {
            return '0px';
        }
    }

    openPreview(index: number): void {
        this.logService.log('NgxGalleryComponent.openPreview(' + this.name + ')', 'index: ' + index);
        // this could be simplified through previewCustom
        if (this.currentOptions && this.currentOptions.ccpImageAsLink && this.links[index]) {
            window.open(this.links[index], '_blank');
        } else if (this.currentOptions && this.currentOptions.previewCustom) {
            this.currentOptions.previewCustom(index);
        } else {
            this.previewEnabled = true;
            // TODO: do we need this?
            this.styleBox = {};
            this.logService.log('NgxGalleryComponent.openPreview(' + this.name + ')', 'previewEnabled: ' + this.previewEnabled);

            if (this.previewLoader) {
                this.logService.log('NgxGalleryComponent.openPreview(' + this.name + ')', 'preview called ...');
                // TODO: do we need
                this.prevIndex = index;
                this.previewLoader.loadPreview();
            } else {
                this.logService.log('NgxGalleryComponent.openPreview(' + this.name + ')', 'preview does not exist!!');
                if (this.images) {
                    this.logService.log('NgxGalleryComponent.openPreview(' + this.name + ')', 'id: ' + this.images[index].id);
                    this.logService.log('NgxGalleryComponent.openPreview(' + this.name + ')', 'bigimage.url: ' + this.bigImages[index]);
                }
            }
        }
    }

    onPreviewOpen(): void {
        this.previewOpen.emit();

        if (this.image && this.image.autoPlay) {
            this.image.stopAutoPlay();
        }
    }

    onPreviewClose(): void {
        this.previewEnabled = false;
        this.styleBox = this.currentOptions.ccpBoxLayout;
        this.logService.log('NgxGalleryComponent.onPreviewClose(' + this.name + ')', 'previewEnabled: ' + this.previewEnabled);
        this.previewClose.emit();

        if (this.image && this.image.autoPlay) {
            this.image.startAutoPlay();
        }
    }

    selectFromImage(index: number) {
        this.select(index);
    }

    selectFromThumbnails(index: number) {
        this.select(index);

        if (this.currentOptions && this.currentOptions.thumbnails && this.currentOptions.preview
            && (!this.currentOptions.image || this.currentOptions.thumbnailsRemainingCount)) {
            // this.openPreview(this.selectedIndex);
            this.logService.log('NgxGalleryComponent.selectFromThumbnails(' + this.name + ')', 'openPreview ...');
        } else {
            this.logService.log('NgxGalleryComponent.selectFromThumbnails(' + this.name + ')', '...');
        }
    }

    show(index: number): void {
        this.logService.log('NgxGalleryComponent.show(' + this.name + ')', '...');
        this.select(index);
    }

    showNext(): void {
        this.image.showNext();
    }

    showPrev(): void {
        this.image.showPrev();
    }

    canShowNext(): boolean {
        if (this.images && this.currentOptions) {
            return (this.currentOptions.imageInfinityMove || this.selectedIndex < this.images.length - 1)
                ? true : false;
        } else {
            return false;
        }
    }

    canShowPrev(): boolean {
        if (this.images && this.currentOptions) {
            return (this.currentOptions.imageInfinityMove || this.selectedIndex > 0) ? true : false;
        } else {
            return false;
        }
    }

    previewSelect(index: number) {
        this.show(index);
        this.previewChange.emit({ index, image: this.images[index] });
    }

    moveThumbnailsRight() {
        this.thubmnails.moveRight();
    }

    moveThumbnailsLeft() {
        this.thubmnails.moveLeft();
    }

    canMoveThumbnailsRight() {
        return this.thubmnails.canMoveRight();
    }

    canMoveThumbnailsLeft() {
        return this.thubmnails.canMoveLeft();
    }

    private initOptions(): void {
        this.options = this.customOptions.getOptions().map((opt) => new NgxGalleryOptions(opt));
        this.sortOptions();
        this.setBreakpoint();
        this.setOptions();
        this.styleBox = this.currentOptions.ccpBoxLayout;
        this.checkFullWidth();
        if (this.currentOptions) {
            this.selectedIndex = this.currentOptions.startIndex;
        }

        if (this.currentOptions && this.currentOptions.previewFullSize) {
            this.logService.log('NgxGalleryComponent.ngOnInit',
                'TODO: implement previewFullSize (show image in full size and don\'t scale it down)');
        }
    }

    private initImages(): void {
        this.images = this.ccMediaService.prepareMedia(this.images, this.customOptions.dimensions);
        this.setImages();

        if (this.images && this.images.length) {
            this.imagesReady.emit();
        }

        if (this.image) {
            this.image.reset(this.currentOptions.startIndex);
        }

        this.resetThumbnails();
    }

    private resetThumbnails() {
        if (this.thubmnails) {
            this.thubmnails.reset(<number>this.currentOptions.startIndex);
        }
    }

    private select(index: number) {
        this.logService.log('NgxGalleryComponent.select(' + this.name + ')',
            'index - ' + index + ' / selectedindex - ' + this.selectedIndex);
        let isNewMM = this.selectedIndex !== index;

        if (isNewMM) {
            this.selectedIndex = index;
            this.prepareSvgAndPdf();
        }

        this.change.emit({
            index,
            image: this.images[index]
        });
    }

    private checkFullWidth(): void {
        if (this.currentOptions && this.currentOptions.fullWidth) {
            this.width = document.body.clientWidth + 'px';
            this.left = (-(document.body.clientWidth -
                this.myElement.nativeElement.parentNode.innerWidth) / 2) + 'px';
        }
    }

    private setImages(): void {
        let time = new Date().getMilliseconds();
        this.smallImages = this.images.map((img) => <string>img.small);
        this.mediumImages = this.images.map((img, index) => new NgxGalleryOrderedImage({
            id: Md5.hashStr(img.id + '_' + time).toString(),
            src: img.medium,
            autoplay: img.autoplay,
            loop: img.loop,
            poster: img.poster,
            urls: img.urls,
            index: index,
            addOns: img.addOns,
        }));
        this.logService.log('NgxGalleryComponent.setImages(' + this.name + ')', 'mediumImages initialized ...');
        this.bigImages = this.images.map(img => img.big);
        // TODO: html encoding - other possibility to decode?
        // TODO: why do we need this? couldn't we just use an own wrapper?
        this.descriptions = this.images.map(img => (img.description ? img.description.replace(/["'\r\n]/g, '') : ''));
        this.links = this.images.map(img => img.url);
        this.labels = this.images.map(img => img.label);
        this.types = this.images.map(img => img.type);
        this.widths = this.images.map(img => img.width);
        this.heights = this.images.map(img => img.height);
        this.mtypes = this.images.map(img => this.ccMediaService.getMultimediaType(img.type));
        this.hasVideo = this.ccMediaService.containsVideoOrAudio(this.types);


        this.mediaInfos = this.images.map(img => {
            const media = {
                link: img.url,
                type: img.type,
                width: img.width,
                height: img.height,
                bwidth: 0,
                bheight: 0,
            };
            if (this.ccMediaService.isAudio(img.type)) {
                media.bwidth = this.currentOptions.ccpImageWidth;
                // media['bheight'] = this.currentOptions.ccpImageHeight - 80;
                media.bheight = 60;
            } else if (this.ccMediaService.isVideo(img.type)) {
                // TODO: should the 80 be configurable?
                if (img.width > this.currentOptions.ccpImageWidth || img.height > this.currentOptions.ccpImageHeight - 80) {
                    media.bwidth = this.currentOptions.ccpImageWidth;
                    media.bheight = this.currentOptions.ccpImageHeight - 80;
                } else {
                    media.bwidth = img.width || this.currentOptions.ccpImageWidth;
                    media.bheight = img.height || this.currentOptions.ccpImageHeight - 80;
                }
            }
            return media;
        });

        // start of initialization svg and video: momentarily not active
        // if (this.types[0] === 'image/svg' || true) {
        // special video
        // image main screen initialization
        this.imageWidth = [];
        this.imageHeight = [];
        this.positionY = [];
        this.positionX = [];
        this.imageVBox = [];

        let i: number;
        for (i = 0; i < this.images.length; i++) {
            let maxheight = this.currentOptions.ccpImageHeight;
            let maxwidth = this.currentOptions.ccpImageWidth;
            let imgheight = this.images[i].height;
            let imgwidth = this.images[i].width;

            if (imgwidth === 0) {
                this.imageWidth[i] = 0;
                this.imageHeight[i] = 0;

                this.positionY[i] = 0;
                this.positionX[i] = 0;
                this.imageVBox[i] = '0 0 ' + imgwidth + ' ' + imgheight;

            } else {
                if (maxheight / imgheight < maxwidth / imgwidth) {
                    this.imageHeight[i] = Math.min(maxheight, imgheight);
                    this.imageWidth[i] = Math.round(imgwidth * this.imageHeight[i] / imgheight);
                } else {
                    this.imageWidth[i] = Math.min(maxwidth, imgwidth);
                    this.imageHeight[i] = Math.round(imgheight * this.imageWidth[i] / imgwidth);
                }

                this.positionY[i] = Math.round((maxheight - this.imageHeight[i]) / 2);
                this.positionX[i] = Math.round((maxwidth - this.imageWidth[i]) / 2);
                this.imageVBox[i] = '0 0 ' + imgwidth + ' ' + imgheight;
            }
            this.logService.log('NgxGalleryComponent.setImages(' + this.name + ')',
                i + ': id - ' + this.images[i].id + ' / type - ' + this.types[i] + ' / height - ' + imgheight + '/' + maxheight + '/'
                + this.imageHeight[i] + '/' + this.positionY[i]);
            this.logService.log('NgxGalleryComponent.setImages(' + this.name + ')',
                i + ': width - ' + imgwidth + '(imgwidth)/' + maxwidth + '(maxwidth)/'
                + this.imageWidth[i] + '(calc: imageWidth)/' + this.positionX[i] + '(positionX)');
        }
        // end of initialization svg and video: momentarily not active
        // }
    }

    private setBreakpoint(): void {
        this.prevBreakpoint = this.breakpoint;
        let breakpoints;

        if (typeof window !== 'undefined') {
            breakpoints = this.options.filter((opt) => opt.breakpoint >= window.innerWidth)
                .map((opt) => opt.breakpoint);
        }

        if (breakpoints && breakpoints.length) {
            this.breakpoint = breakpoints.pop();
        } else {
            this.breakpoint = undefined;
        }
        this.logService.log('NgxGalleryComponent.setBreakpoint(' + this.name + ')', 'breakpoint - ' + this.breakpoint);
    }

    private sortOptions(): void {
        this.options = [
            ...this.options.filter((a) => a.breakpoint === undefined),
            ...this.options
                .filter((a) => a.breakpoint !== undefined)
                .sort((a, b) => b.breakpoint - a.breakpoint)
        ];
    }

    private setOptions(): void {
        this.currentOptions = new NgxGalleryOptions({});

        this.options
            .filter((opt) => opt.breakpoint === undefined || opt.breakpoint >= this.breakpoint)
            .map((opt) => {
                this.logService.log('NgxGalleryComponent.setOptions(' + this.name + ')', '' + opt.breakpoint);
                this.combineOptions(this.currentOptions, opt)
            });

        this.width = <string>this.currentOptions.width;
        this.height = <string>this.currentOptions.height;
    }

    private combineOptions(first: NgxGalleryOptions, second: NgxGalleryOptions) {
        Object.keys(second).map((val) => {
            if (val === 'actions' || val === 'imageActions') {
                if (second[val] !== undefined && second[val].find(obj => { return obj.icon === this.pdfAction.icon }) === undefined) {
                    second[val].push(this.pdfAction);
                    second[val].push(this.svgAction);
                    if (val === 'imageActions') {
                        second[val].push(this.prevAction);
                    }
                } else if (first[val].find(obj => { return obj.icon === this.pdfAction.icon }) === undefined) {
                    first[val].push(this.pdfAction);
                    first[val].push(this.svgAction);
                    if (val === 'imageActions') {
                        first[val].push(this.prevAction);
                    }
                }
            }
            first[val] = second[val] !== undefined ? second[val] : first[val];
        });
    }

    // icon initialization
    prepareSvgAndPdf(): void {
        this.logService.log('NgxGalleryComponent.initCmd(' + this.name + ')', 'starts ...');
        this.updateSvgState(this.selectedIndex);
        this.initPDF();
    }

    private initActions(): void {
        this.pdfAction = this.pdfAction || {
            icon: 'fa fa-file-pdf-o',
            disabled: !this.isPDF,
            onClick: this.openPDF.bind(this),
            titleText: 'pdf'
        };
        this.svgAction = this.svgAction || {
            icon: 'fa fa-map-marker',
            // TODO use method / variable on current image to get this information
            disabled: !this.isSVG,
            onClick: this.toggleActivation.bind(this),
            titleText: 'interactive'
        };
        this.prevAction = this.prevAction || {
            icon: 'fa fa-expand',
            onClick: this.openIconPreview.bind(this),
            titleText: 'full screen'
        };
    }

    updateSvgState(selectedIndex: number): void {
        this.isSVG = this.images[selectedIndex] &&
            this.images[selectedIndex].addOns &&
            !!this.images[selectedIndex].addOns.length;
        if (this.svgAction) {
            this.svgAction.disabled = !this.isSVG;
        }
        this.logService.log('NgxGalleryComponent.updateSvgState(' + this.name + ')', 'selectedIndex - ' + selectedIndex);
    }

    initPDF(): void {
        this.isPDF = this.ccMediaService.isPDF(this.images[this.selectedIndex ? this.selectedIndex : 0].type);
        if (this.pdfAction) {
            this.pdfAction.disabled = !this.isPDF;
        }
        this.logService.log('NgxGalleryComponent.initPDF(' + this.name + ')', 'selectedIndex - ' + this.selectedIndex);
    }

    // icon click actions ...
    openPDF(event, index): void {
        this.logService.log('NgxGalleryComponent.openPDF', index + ': ' + this.images[index].url);
        window.open(this.images[index].url, '_blank');
    }

    openIconPreview(event, index): void {
        this.logService.log('NgxGalleryComponent.openIconPreview(' + this.name + ')', 'index - ' + index);
        this.openPreview(this.image.selectedIndex);
    }

    // TODO: rename to toggleOverlays
    toggleActivation(): void {
        // TODO: rename to overlaysVisible or similar
        this.isSVGActive = !this.isSVGActive;
        this.logService.log('NgxGalleryComponent.toggleActivation(' + this.name + ')', 'isSVGActive - ' + this.isSVGActive);
        // this.setSVG();
    }

    // get multimedia functions ...
    isExternalVideo(mtype: string): boolean {
        return this.ccMediaService.isExternalVideo(mtype);
    }
}
