import {
    Component, Input, Output, EventEmitter, ElementRef, OnInit, OnChanges,
    SimpleChanges
} from '@angular/core';
import { DomSanitizer, SafeStyle, SafeUrl } from '@angular/platform-browser';

import { NgxGalleryHelperService } from './ngx-gallery-helper.service';
import { NgxGalleryOrderedImage } from './ngx-gallery-ordered-image.model';
import { NgxGalleryAnimation } from './ngx-gallery-animation.model';
import { NgxGalleryAction } from './ngx-gallery-action.model';

import { LogService } from './utils/logging.service';

@Component({
    selector: 'ngx-gallery-image',
    template: `
        <div
            class="ngx-gallery-image-wrapper ngx-gallery-animation-{{animation}} ngx-gallery-image-size-{{size}}"
            *ngFor="let image of getImages(); let i = index;"
        >
            <div
                *ngIf="mtypes[image.index] === 'image' || mtypes[image.index] === 'pdf'"
                class="ngx-gallery-image"
                [ngClass]="{ 'ngx-gallery-active': selectedIndex == image.index,
                            'ngx-gallery-inactive-left': selectedIndex > image.index,
                            'ngx-gallery-inactive-right': selectedIndex < image.index,
                            'ngx-gallery-clickable': clickable }"
                [style.background-image]="getSafeUrl(image.src)"
                [style.background-size]="getBackgroundSize(image.index)"
                (click)="handleClick($event, image.index)"
            >
                <div class="ngx-gallery-icons-wrapper ngx-gallery-actionicon-image">
                <ng-container *ngFor="let action of actions">
                    <ngx-gallery-action
                        *ngIf="!action.disabled"
                        [icon]="action.icon"
                        [disabled]="action.disabled"
                        [titleText]="action.titleText"
                        (onClick)="action.onClick($event, image.index)">
                    </ngx-gallery-action>
                </ng-container>
                </div>

                <cc-imagesvg
                    *ngIf="isSVGActive && !!image.addOns.length"
                    [animation]="animation"
                    [width]="imageWidth[image.index]"
                    [height]="imageHeight[image.index]"
                    [viewBox]="imageVBox[image.index]"
                    [posLeft]="positionX[image.index] + 'px'"
                    [posTop]="positionY[image.index] + 'px'"
                    [zoomValue]="zoomValue"
                    [addOns]="image.addOns"
                >
                </cc-imagesvg>
                <div class="ngx-gallery-image-text" *ngIf="showDescription && descriptions[image.index]"
                            [innerHTML]="descriptions[image.index]" (click)="$event.stopPropagation()"></div>
            </div>

            <cc-video
                *ngIf="mtypes[image.index] === 'audio' || mtypes[image.index] === 'video'"
                [selectedIndex]="selectedIndex"
                [image]="image"
                [showDescription]="showDescription"
                [description]="showDescription ? descriptions[image.index] : ''"
                [mediaInfo]="mediaInfos[image.index]"
            ></cc-video>

        </div>
        <ngx-gallery-bullets
            *ngIf="bullets"
            [count]="images.length"
            [active]="selectedIndex"
            (onChange)="show($event)">
        </ngx-gallery-bullets>
        <ngx-gallery-arrows
            class="ngx-gallery-image-size-{{size}}"
            *ngIf="arrows"
            (onPrevClick)="showPrev()"
            (onNextClick)="showNext()"
            [prevDisabled]="!canShowPrev()"
            [nextDisabled]="!canShowNext()"
            [arrowPrevIcon]="arrowPrevIcon"
            [arrowNextIcon]="arrowNextIcon">
        </ngx-gallery-arrows>
    `,
    styleUrls: ['./ngx-gallery-image.component.scss']
})

export class NgxGalleryImageComponent implements OnInit, OnChanges {

    zoomValue = '1.0';

    @Input() name: string;
    @Input() images: NgxGalleryOrderedImage[];
    @Input() imageWidth: number[];
    @Input() imageHeight: number[];
    @Input() imageVBox: string[];
    @Input() positionX: number[];
    @Input() positionY: number[];
    @Input() mediaInfos: Object[];
    @Input() hasVideo: boolean;
    @Input() links: string[];
    @Input() clickable: boolean;
    @Input() selectedIndex: number;
    @Input() arrows: boolean;
    @Input() arrowsAutoHide: boolean;
    @Input() swipe: boolean;
    @Input() animation: string;
    @Input() size: string;
    @Input() arrowPrevIcon: string;
    @Input() arrowNextIcon: string;
    @Input() autoPlay: boolean;
    @Input() autoPlayInterval: number;
    @Input() autoPlayPauseOnHover: boolean;
    @Input() infinityMove: boolean;
    @Input() lazyLoading: boolean;
    @Input() actions: NgxGalleryAction[];
    @Input() descriptions: string[];
    @Input() types: string[];
    @Input() mtypes: string[];
    @Input() widths: number[];
    @Input() heights: number[];
    @Input() showDescription: boolean;
    @Input() bullets: boolean;

    @Input() interactive: boolean;
    @Input() isSVGActive: boolean;
    @Input() interactiveIcon: string;
    @Input() pdfIcon: string;


    @Output() onClick = new EventEmitter();
    @Output() onActiveChange = new EventEmitter();

    canChangeImage = true;
    countChanges = 0;

    private timer;

    constructor(private sanitization: DomSanitizer,
        private elementRef: ElementRef, private helperService: NgxGalleryHelperService, private logService: LogService) {
    }

    ngOnInit(): void {
        if (this.arrows && this.arrowsAutoHide) {
            this.arrows = false;
        }

        if (this.autoPlay) {
            this.startAutoPlay();
        }
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['swipe']) {
            this.helperService.manageSwipe(this.swipe, this.elementRef, 'image', () => this.showNext(), () => this.showPrev());
        }

        /*
        for (let propName in changes) {
            if (changes.hasOwnProperty(propName)) {
                let change = changes[propName];
                let curVal  = JSON.stringify(change.currentValue);
                let prevVal = JSON.stringify(change.previousValue);
            }
        }
        */

        if (changes.hasOwnProperty('selectedIndex')) {
            this.countChanges++;
            this.logService.log('NgxGalleryImageComponent.ngOnChanges(' + this.name + ')', 'starts ...');
        }

    }

    //    currently resizing window does NOT lead to a resize event, if external video in an iframe is shown
    //    problem: otherwise video would be reloaded after leaving the mm box!

    /*
    TODO: Why do we need to disable this???
    @HostListener('mouseenter') onMouseEnter() {
        if (this.arrowsAutoHide && !this.arrows) {
            this.arrows = true;
        }

        if (this.autoPlay && this.autoPlayPauseOnHover) {
            this.stopAutoPlay();
        }
    }

    @HostListener('mouseleave') onMouseLeave() {
        if (this.arrowsAutoHide && this.arrows) {
            this.arrows = false;
        }

        if (this.autoPlay && this.autoPlayPauseOnHover) {
            this.startAutoPlay();
        }
    }
    */

    reset(index: number): void {
        this.selectedIndex = index;
    }

    getImages(): NgxGalleryOrderedImage[] {
        if (!this.images) {
            return [];
        }

        if (this.lazyLoading) {
            let indexes = [this.selectedIndex];
            let prevIndex = this.selectedIndex - 1;

            if (prevIndex === -1 && this.infinityMove) {
                indexes.push(this.images.length - 1)
            } else if (prevIndex >= 0) {
                indexes.push(prevIndex);
            }

            let nextIndex = this.selectedIndex + 1;

            if (nextIndex === this.images.length && this.infinityMove) {
                indexes.push(0);
            } else if (nextIndex < this.images.length) {
                indexes.push(nextIndex);
            }

            return this.images.filter((img, i) => indexes.indexOf(i) !== -1);
        } else {
            return this.images;
        }
    }

    startAutoPlay(): void {
        this.stopAutoPlay();

        this.timer = setInterval(() => {
            if (!this.showNext()) {
                this.selectedIndex = -1;
                this.showNext();
            }
        }, this.autoPlayInterval);
    }

    stopAutoPlay() {
        if (this.timer) {
            clearInterval(this.timer);
        }
    }

    handleClick(event: Event, index: number): void {
        if (this.clickable) {
            this.onClick.emit(index);

            event.stopPropagation();
            event.preventDefault();
        }
    }

    show(index: number) {
        this.selectedIndex = index;
        this.onActiveChange.emit(this.selectedIndex);
        this.setChangeTimeout();
    }

    showNext(): boolean {
        if (this.canShowNext() && this.canChangeImage) {
            this.selectedIndex++;

            if (this.selectedIndex === this.images.length) {
                this.selectedIndex = 0;
            }

            this.onActiveChange.emit(this.selectedIndex);
            this.setChangeTimeout();

            return true;
        } else {
            return false;
        }
    }

    showPrev(): void {
        if (this.canShowPrev() && this.canChangeImage) {
            this.selectedIndex--;

            if (this.selectedIndex < 0) {
                this.selectedIndex = this.images.length - 1;
            }

            this.onActiveChange.emit(this.selectedIndex);
            this.setChangeTimeout();
        }
    }

    setChangeTimeout() {
        this.canChangeImage = false;
        let timeout = 1000;

        if (this.animation === NgxGalleryAnimation.Slide
            || this.animation === NgxGalleryAnimation.Fade) {
            timeout = 500;
        }

        setTimeout(() => {
            this.canChangeImage = true;
        }, timeout);
    }

    canShowNext(): boolean {
        if (this.images) {
            return this.infinityMove || this.selectedIndex < this.images.length - 1
                ? true : false;
        } else {
            return false;
        }
    }

    canShowPrev(): boolean {
        if (this.images) {
            return this.infinityMove || this.selectedIndex > 0 ? true : false;
        } else {
            return false;
        }
    }

    getSafeUrl(imageUrl: string): SafeStyle {
        return this.sanitization.bypassSecurityTrustStyle(this.helperService.getBackgroundUrl(imageUrl));
    }

    getBackgroundSize(index: number): string {
        return (this.mtypes[index] === 'pdf' ? '50px' : '100%');
    }
}
