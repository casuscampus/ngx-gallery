import {
    Component, Input, Output, EventEmitter, OnChanges, OnInit,
    SimpleChanges, ElementRef, ViewChild, Renderer, OnDestroy, HostBinding
} from '@angular/core';
import { SafeResourceUrl, DomSanitizer, SafeUrl, SafeStyle } from '@angular/platform-browser';

import { NgxGalleryAction } from './ngx-gallery-action.model';
import { NgxGalleryHelperService } from './ngx-gallery-helper.service';

import { NgxGalleryOrderedImage } from './ngx-gallery-ordered-image.model';
import { LogService } from './utils/logging.service';
import { CcMediaService } from './utils/cc-media.service';

@Component({
    selector: 'ngx-gallery-preview',
    template: `
        <ngx-gallery-arrows
            (onPrevClick)="showPrev()"
            (onNextClick)="showNext()"
            [prevDisabled]="!canShowPrev()"
            [nextDisabled]="!canShowNext()"
            [arrowPrevIcon]="arrowPrevIcon"
            [arrowNextIcon]="arrowNextIcon"
        ></ngx-gallery-arrows>

        <div class="ngx-gallery-preview-top">
            <div class="ngx-gallery-preview-icons">
            <ng-container *ngFor="let action of actions">
                <ngx-gallery-action
                    *ngIf="!action.disabled"
                    [icon]="action.icon"
                    [disabled]="action.disabled"
                    [titleText]="action.titleText"
                    (onClick)="action.onClick($event, index)">
                </ngx-gallery-action>
            </ng-container>

                <a *ngIf="download && src" [href]="src" class="ngx-gallery-icon" aria-hidden="true" download>
                    <i class="ngx-gallery-icon-content {{ downloadIcon }}"></i>
                </a>
                <ngx-gallery-action *ngIf="zoom" [icon]="zoomOutIcon" [disabled]="! isZoomableOut" (onClick)="zoomOut()"></ngx-gallery-action>
                <ngx-gallery-action *ngIf="zoom" [icon]="zoomInIcon" [disabled]="! isZoomableIn" (onClick)="zoomIn()"></ngx-gallery-action>
                <ngx-gallery-action *ngIf="rotate" [icon]="rotateLeftIcon" (onClick)="rotateLeft()"></ngx-gallery-action>
                <ngx-gallery-action *ngIf="rotate" [icon]="rotateRightIcon" (onClick)="rotateRight()"></ngx-gallery-action>
                <ngx-gallery-action *ngIf="fullscreen" [icon]="'ngx-gallery-fullscreen ' + fullscreenIcon"
                    (onClick)="manageFullscreen()">
                </ngx-gallery-action>
                <ngx-gallery-action [icon]="'ngx-gallery-close ' + closeIcon" (onClick)="close()"></ngx-gallery-action>
            </div>
        </div>
        <div class="ngx-spinner-wrapper ngx-gallery-center" [class.ngx-gallery-active]="showSpinner">
            <i class="ngx-gallery-icon ngx-gallery-spinner {{spinnerIcon}}" aria-hidden="true"></i>
        </div>

        <ng-container
            *ngIf="mtypes[index] === 'video' || mtypes[index] === 'audio'; then prevVideo; else other"
        ></ng-container>

        <ng-template #other>
            <ng-container
                *ngIf="mtypes[index] === 'campus'; then campus; else img"
            ></ng-container>
        </ng-template>

        <ng-template #campus>
            <iframe
                *ngIf="isOpen"
                [src]="src"
            ></iframe>
        </ng-template>

        <ng-template #img>
            <div
                class="ngx-gallery-preview-wrapper"
                (click)="closeOnClick && close()"
                (mouseup)="mouseUpHandler($event)" (mousemove)="mouseMoveHandler($event)"
                (touchend)="mouseUpHandler($event)" (touchmove)="mouseMoveHandler($event)"

                (click)="$event.stopPropagation()"
                (mouseenter)="imageMouseEnter()"
                (mouseleave)="imageMouseLeave()"
                (mousedown)="mouseDownHandler($event)"
                (touchstart)="mouseDownHandler($event)"
            >
                <img
                    #previewImage
                    *ngIf="src"
                    class="ngx-gallery-preview-img ngx-gallery-center"

                    [src]="src"
                    [class.ngx-gallery-active]="!loading"
                    [class.ngx-gallery-grab]="canDrag"
                    [style.transform]="styleTransform"
                    [style.left]="positionLeft + 'px'"
                    [style.top]="positionTop + 'px'"

                    (click)="$event.stopPropagation()"
                />

                <cc-imagesvg
                    *ngIf="isSVGActive && hasAddOns"
                    [animation]="animation"
                    [width]="imgWidth"
                    [height]="imgHeight"
                    [viewBox]="viewBox"
                    [styleTransform]="styleTransform"
                    [posLeft]="positionLeft + 'px'"
                    [posTop]="positionTop + 'px'"
                    [zoomValue]="zoomValue"
                    [addOns]="ordImages[prevIndex].addOns"
                    [classGrab]="canDrag"
                    [preview]="true"
                >
                </cc-imagesvg>

                <ngx-gallery-bullets *ngIf="bullets" [count]="images.length" [active]="index" (onChange)="showAtIndex($event)">
                </ngx-gallery-bullets>

                <div class="ngx-gallery-preview-text" *ngIf="showDescription && description" [innerHTML]="description" (click)="$event.stopPropagation()">
                </div>
            </div>
        </ng-template>

        <ng-template #prevVideo>
            <div
                class="ngx-gallery-preview-wrapper"
                (click)="$event.stopPropagation()"
            >
                <cc-video
                    *ngIf="isOpen"
                    [selectedIndex]="prevIndex"
                    [image]="ordImages[prevIndex]"
                    [showDescription]="showDescription"
                    [description]="descriptions[prevIndex]"
                    [mediaInfo]="mediaInfos[prevIndex]"
                ></cc-video>
                <!--
                <div
                    *ngIf="showDescription && description"
                    class="ngx-gallery-preview-text-doedel"
                    [innerHTML]="description"
                    (click)="$event.stopPropagation()"
                ></div>
                -->
            </div>
        </ng-template>
    `,
    styleUrls: ['./ngx-gallery-preview.component.scss']
})
export class NgxGalleryPreviewComponent implements OnChanges, OnInit, OnDestroy {

    src: SafeUrl | SafeResourceUrl;
    srcType: string;
    imgWidth: number;
    imgHeight: number;
    description: string;
    viewBox: string;
    showSpinner = false;
    positionLeft = 0;
    positionTop = 0;
    zoomValue = 1;
    isZoomableIn = false;
    isZoomableOut = false;
    styleTransform: SafeStyle;
    canDrag: boolean;
    loading = false;
    rotateValue = 0;
    index = 0;
    hasAddOns = false;

    @Input() prevIndex: number;
    @Input() images: string[] | SafeResourceUrl[];
    @Input() ordImages: NgxGalleryOrderedImage[];
    @Input() mediaInfos: Object[];
    @Input() links: string[];
    @Input() descriptions: string[];
    @Input() types: string[];
    @Input() mtypes: string[];
    @Input() widths: number[];
    @Input() heights: number[];
    @Input() showDescription: boolean;
    @Input() swipe: boolean;
    @Input() fullscreen: boolean;
    @Input() forceFullscreen: boolean;
    @Input() closeOnClick: boolean;
    @Input() closeOnEsc: boolean;
    @Input() keyboardNavigation: boolean;
    @Input() arrowPrevIcon: string;
    @Input() arrowNextIcon: string;
    @Input() closeIcon: string;
    @Input() fullscreenIcon: string;
    @Input() spinnerIcon: string;
    @Input() autoPlay: boolean;
    @Input() autoPlayInterval: number;
    @Input() autoPlayPauseOnHover: boolean;
    @Input() infinityMove: boolean;
    @Input() interactive: boolean;
    @Input() isSVGActive: boolean;
    @Input() zoom: boolean;
    @Input() zoomStep: number;
    @Input() zoomMax: number;
    @Input() zoomMin: number;
    @Input() interactiveIcon: string;
    @Input() zoomInIcon: string;
    @Input() zoomOutIcon: string;
    @Input() animation: boolean;
    @Input() actions: NgxGalleryAction[];
    @Input() rotate: boolean;
    @Input() rotateLeftIcon: string;
    @Input() rotateRightIcon: string;
    @Input() download: boolean;
    @Input() downloadIcon: string;
    @Input() bullets: string;
    @Input() name: string;

    @Output() onOpen = new EventEmitter();
    @Output() onClose = new EventEmitter();
    @Output() onActiveChange = new EventEmitter<number>();

    @HostBinding('class.ngx-gallery-active') isActive: boolean;

    @ViewChild('previewImage') previewImage: ElementRef;

    isOpen = false;
    private timer;
    private initialX = 0;
    private initialY = 0;
    private initialLeft = 0;
    private initialTop = 0;
    private isMove = false;

    private keyDownListener: Function;

    constructor(
        private sanitization: DomSanitizer,
        private elementRef: ElementRef,
        private helperService: NgxGalleryHelperService,
        private renderer: Renderer,
        private logService: LogService,
        public ccMediaService: CcMediaService
    ) { }

    ngOnInit() {
        this.logService.log('NgxGalleryPreviewComponent.ngOnInit (' + this.name + ')', '...');
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (!changes.isFirstChange) {
            let prevIndexChanged = false;
            this.logService.log('NgxGalleryPreviewComponent.ngOnChange (' + this.name + ')', '...');
            this.styleTransform = this.getTransform();
            this.canDrag = this.canDragOnZoom();
            this.isZoomableIn = this.canZoomIn();
            this.isZoomableOut = this.canZoomOut();

            if (changes['swipe']) {
                this.helperService.manageSwipe(this.swipe, this.elementRef,
                    'preview', () => this.showNext(), () => this.showPrev());
            }

            prevIndexChanged = (changes.prevIndex && !changes.prevIndex.isFirstChange()
                && changes.prevIndex.currentValue > -1);
            const imageChanged = (changes.images);

            if (imageChanged || prevIndexChanged) {
                if (prevIndexChanged) {
                    this.index = this.prevIndex;
                    this.open(this.prevIndex);
                }
                this.logService.log('NgxGalleryPreviewComponent.ngOnChange (' + this.name + ')', 'prevIndex: ' + this.prevIndex);
            } else {
                this.logService.log('NgxGalleryPreviewComponent.ngOnChange (' + this.name + ')', 'changes.prevIndex: ' + changes.prevIndex);
            }
            this.logService.log('NgxGalleryPreviewComponent.ngOnChange (' + this.name + ')', 'images[0]: ' + (this.images && this.images[0]));
        }
    }

    ngOnDestroy() {
        this.logService.log('NgxGalleryPreviewComponent.ngOnDestroy (' + this.name + ')', '...');
        if (this.keyDownListener) {
            this.keyDownListener();
        }
    }

    onKeyDown(e) {
        if (this.isOpen) {
            if (this.keyboardNavigation) {
                if (this.isKeyboardPrev(e)) {
                    this.showPrev();
                } else if (this.isKeyboardNext(e)) {
                    this.showNext();
                }
            }
            if (this.closeOnEsc && this.isKeyboardEsc(e)) {
                this.close();
            }
        }
    }

    open(index: number): void {
        this.logService.log('NgxGalleryPreviewComponent.open(' + this.name + ')', 'starts ... ' + index);
        this.logService.log('NgxGalleryPreviewComponent.open(' + this.name + ')', 'image: ' + this.images[index]);
        this.onOpen.emit();

        this.index = index;
        this.isOpen = true;
        this.show(true);

        if (this.forceFullscreen) {
            this.manageFullscreen();
        }

        this.keyDownListener = this.renderer.listenGlobal('window', 'keydown', (e) => this.onKeyDown(e));
    }

    close(): void {
        this.isOpen = false;
        this.closeFullscreen();
        this.onClose.emit();

        this.stopAutoPlay();

        if (this.keyDownListener) {
            this.keyDownListener();
        }
    }

    imageMouseEnter(): void {
        if (this.autoPlay && this.autoPlayPauseOnHover) {
            this.stopAutoPlay();
        }
    }

    imageMouseLeave(): void {
        if (this.autoPlay && this.autoPlayPauseOnHover) {
            this.startAutoPlay();
        }
    }

    startAutoPlay(): void {
        if (this.autoPlay) {
            this.stopAutoPlay();

            this.timer = setTimeout(() => {
                if (!this.showNext()) {
                    this.index = -1;
                    this.showNext();
                }
            }, this.autoPlayInterval);
        }
    }

    stopAutoPlay(): void {
        if (this.timer) {
            clearTimeout(this.timer);
        }
    }

    showAtIndex(index: number): void {
        this.index = index;
        this.show();
    }

    showNext(): boolean {
        if (this.canShowNext()) {
            this.index = this.getNextImageIndex();
            this.show();
            return true;
        } else {
            return false;
        }
    }

    showPrev(): void {
        if (this.canShowPrev()) {
            this.index = this.getPrevImageIndex();
            this.show();
        }
    }

    canShowNext(): boolean {
        if (this.loading) {
            return false;
        } else if (this.images) {
            return this.getNextImageIndex() !== undefined;
        } else {
            return false;
        }
    }

    canShowPrev(): boolean {
        if (this.loading) {
            return false;
        } else if (this.images) {
            return this.getPrevImageIndex() !== undefined;
        } else {
            return false;
        }
    }

    private getNextImageIndex(): number {
        const start = this.index + 1;
        const end = this.infinityMove ? this.images.length + this.index - 1 : this.images.length;
        for (let i = start; i < end; i++) {
            const index = i % this.images.length;
            if (this.isIterableImage(index, this.mtypes)) {
                return index;
            }
        };
        return;
    }

    private getPrevImageIndex(): number {
        const start = this.images.length + this.index - 1;
        const end = this.infinityMove ? this.index : this.images.length - 1;
        for (let i = start; i > end; i--) {
            const index = i % this.images.length;
            if (this.isIterableImage(index, this.mtypes)) {
                return index;
            }
        };
        return;
    }

    private isIterableImage(index: number, mediaTypes: string[]): boolean {
        return ['image', 'pdf'].indexOf(mediaTypes[index]) !== -1;
    }

    manageFullscreen(): void {
        if (this.fullscreen || this.forceFullscreen) {
            const doc = <any>document;

            if (!doc.fullscreenElement && !doc.mozFullScreenElement
                && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
                this.openFullscreen();
            } else {
                this.closeFullscreen();
            }
        }
    }

    getSafeUrl(image: string): SafeUrl {
        return !image || image.substr(0, 10) === 'data:image' ?
            image : this.sanitization.bypassSecurityTrustUrl(image);
    }

    getSafeResourceUrl(resUrl: string): SafeResourceUrl {
        return this.sanitization.bypassSecurityTrustResourceUrl(resUrl);
    }

    zoomIn(): void {
        if (this.isZoomableIn) {
            this.zoomValue += this.zoomStep;

            if (this.zoomValue > this.zoomMax) {
                this.zoomValue = this.zoomMax;
            }
            this.styleTransform = this.getTransform();
            this.canDrag = this.canDragOnZoom();
            this.isZoomableIn = this.canZoomIn();
            this.isZoomableOut = this.canZoomOut();
        }
    }

    zoomOut(): void {
        if (this.isZoomableOut) {
            this.zoomValue -= this.zoomStep;

            if (this.zoomValue < this.zoomMin) {
                this.zoomValue = this.zoomMin;
            }

            if (this.zoomValue <= 1) {
                this.resetPosition()
            }
            this.styleTransform = this.getTransform();
            this.canDrag = this.canDragOnZoom();
            this.isZoomableIn = this.canZoomIn();
            this.isZoomableOut = this.canZoomOut();
        }
    }

    rotateLeft(): void {
        this.rotateValue -= 90;
    }

    rotateRight(): void {
        this.rotateValue += 90;
    }

    getTransform(): SafeStyle {
        return this.sanitization.bypassSecurityTrustStyle('scale(' + this.zoomValue + ') rotate(' + this.rotateValue + 'deg)');
    }

    canZoomIn(): boolean {
        return ['pdf', 'campus'].indexOf(this.mtypes[this.index]) === -1 && this.zoomValue < this.zoomMax;
    }

    canZoomOut(): boolean {
        return ['pdf', 'campus'].indexOf(this.mtypes[this.index]) === -1 && this.zoomValue > this.zoomMin;
    }

    canDragOnZoom() {
        return this.zoom && this.zoomValue > 1;
    }

    mouseDownHandler(e): void {
        if (this.canDragOnZoom()) {
            this.initialX = this.getClientX(e);
            this.initialY = this.getClientY(e);
            this.initialLeft = this.positionLeft;
            this.initialTop = this.positionTop;
            this.isMove = true;

            e.preventDefault();
        }
    }

    mouseUpHandler(e): void {
        this.isMove = false;
    }

    mouseMoveHandler(e) {
        if (this.isMove) {
            this.positionLeft = this.initialLeft + (this.getClientX(e) - this.initialX);
            this.positionTop = this.initialTop + (this.getClientY(e) - this.initialY);
        }
    }

    private getClientX(e): number {
        return e.touches && e.touches.length ? e.touches[0].clientX : e.clientX;
    }

    private getClientY(e): number {
        return e.touches && e.touches.length ? e.touches[0].clientY : e.clientY;
    }

    private resetPosition() {
        if (this.zoom) {
            this.positionLeft = 0;
            this.positionTop = 0;
        }
    }

    private isKeyboardNext(e): boolean {
        return e.keyCode === 39 ? true : false;
    }

    private isKeyboardPrev(e): boolean {
        return e.keyCode === 37 ? true : false;
    }

    private isKeyboardEsc(e): boolean {
        return e.keyCode === 27 ? true : false;
    }

    private openFullscreen(): void {
        const element = <any>document.documentElement;

        if (element.requestFullscreen) {
            element.requestFullscreen();
        } else if (element.msRequestFullscreen) {
            element.msRequestFullscreen();
        } else if (element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        } else if (element.webkitRequestFullscreen) {
            element.webkitRequestFullscreen();
        }
    }

    private closeFullscreen(): void {

        const doc = <any>document;
        const fullscreenPolyfills = [
            {
                vendor: 'standard',
                fullscreenElement: 'fullscreenElement',
                exitFullscreenFn: 'exitFullscreen',
            }, {
                vendor: ['blink', 'safari', 'edge'],
                fullscreenElement: 'webkitFullscreenElement',
                exitFullscreenFn: 'webkitExitFullscreen',
            }, {
                vendor: 'gecko',
                fullscreenElement: 'mozFullScreenElement',
                exitFullscreenFn: 'mozCancelFullScreen',
            }, {
                vendor: 'ie11',
                fullscreenElement: 'msFullscreenElement',
                exitFullscreenFn: 'msExitFullscreen',
            }
        ];
        for (let index = 0; index < fullscreenPolyfills.length; index++) {
            const fullscreenPolyfill = fullscreenPolyfills[index];
            if (doc[fullscreenPolyfill.fullscreenElement] !== null && doc[fullscreenPolyfill.exitFullscreenFn]) {
                doc[fullscreenPolyfill.exitFullscreenFn]();
                return;
            }
        }
    }

    private show(first = false) {
        this.loading = true;
        this.stopAutoPlay();

        this.onActiveChange.emit(this.index);

        if (first || !this.animation) {
            this._show();
        } else {
            setTimeout(() => this._show(), 600);
        }
    }

    private _show() {
        this.styleTransform = this.getTransform();
        this.canDrag = this.canDragOnZoom();
        this.rotateValue = 0;
        this.resetPosition();

        this.src = this.mtypes[this.index] === 'campus' ?
            this.getSafeResourceUrl(this.images[this.index] as string) :
            this.getSafeUrl(<string>this.images[this.index]);
        this.description = this.descriptions[this.index];
        this.srcType = this.types[this.index];
        this.imgHeight = this.heights[this.index];
        this.imgWidth = this.widths[this.index];
        this.viewBox = '0 0 ' + this.imgWidth + ' ' + this.imgHeight;
        this.hasAddOns = !!this.ordImages && !!this.ordImages[this.index] && !!this.ordImages[this.index].addOns.length;

        this.logService.log('NgxGalleryPreviewComponent._show', 'index - ' + this.index);
        this.logService.log('NgxGalleryPreviewComponent._show', 'srcType - ' + this.srcType);
        this.logService.log('NgxGalleryPreviewComponent._show', 'url - ' + this.images[this.index]);
        this.logService.log('NgxGalleryPreviewComponent._show', 'src - ' + this.src);
        if (!this.ccMediaService.isImage(this.srcType)) {
            this.zoom = false;
        }


        if (!this.autoPlay) {
            this.loading = false;
            this.showSpinner = false;
        } else {
            setTimeout(() => {
                if (this.isLoaded(this.previewImage.nativeElement)) {
                    this.loading = false;
                    this.startAutoPlay();
                } else {
                    setTimeout(() => {
                        if (this.loading) {
                            this.showSpinner = true;
                        }
                    })
                    if (this.previewImage.nativeElement) {
                        this.previewImage.nativeElement.onload = () => {
                            this.loading = false;
                            this.showSpinner = false;
                            this.previewImage.nativeElement.onload = null;
                            this.startAutoPlay();
                        }
                    }
                }
            })
        }
    }

    private isLoaded(img): boolean {
        if (!img || !img.complete) {
            return false;
        }

        if (typeof img.naturalWidth !== 'undefined' && img.naturalWidth === 0) {
            return false;
        }
        return true;
    }

    hasLink(): boolean {
        return !!(this.links && this.links.length && this.links[this.index]);
    }

    getImages(): NgxGalleryOrderedImage[] {
        if (!this.ordImages) {
            return [];
        }

        return this.ordImages;
    }
}
