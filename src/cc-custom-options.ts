import { NgxGalleryImageSize } from './ngx-gallery-image-size.model';
import { NgxGalleryOptions } from './ngx-gallery-options.model';
import { NgxGalleryDimensions } from './utils/ngx-gallery-dimensions.interface';

export class CcCustomOptions {

  dimensions: NgxGalleryDimensions = {
    box: {width: 0, height: 0},
    image: {width: 0, height: 0},
    thumbnail: {width: 0, height: 0},
  }


  private originalOptions: NgxGalleryOptions[];
  private options: NgxGalleryOptions[];
  private numberOfMedia: number;

  private boxPadding = 10;
  private boxBorder = 3;
  private galleryLayout = {
    'border': this.boxBorder + 'px grey solid',
    'padding': this.boxPadding + 'px',
    // TODO: move into css as it is not dynamic
    'background-color': '#D8D8D8',
  };

  private defaultOptionsLargeScreen: NgxGalleryOptions = {
    width: '400px',  // originally 400
    imageSize: NgxGalleryImageSize.Contain,
    imagePercent: 88.6, // before: 80
    imageDescription: true,
    imageInfinityMove: false,
    imageArrows: false,
    ccpImageAsLink: true,
    image: true,
    lazyLoading: true,
    thumbnailsAsLinks: true,
    ccpPreviewImageAsLink: true,
    previewDescription: true,
    previewZoom: true,
    previewAutoPlay: false, // setting to true needs coding in preview component!
    imageAutoPlay: false, // parameter set by author dynamically TODO
    thumbnailsColumns: 3,
    thumbnailsMoveSize: 3,
    thumbnailSize: NgxGalleryImageSize.Contain,
  };

  private defaultOptionsSmallScreen: NgxGalleryOptions = {
    breakpoint: 800,
    thumbnailsColumns: 2,
    thumbnailsMoveSize: 2,
    image: false, // small screens - do not show image
  };

  constructor(initialOptions: NgxGalleryOptions[], initialNumberOfMedia: number) {
    this.numberOfMedia = initialNumberOfMedia;
    this.originalOptions = initialOptions;
    this.options = this.prepareOptions();
  }

  setOriginalOptions(originalOptions: NgxGalleryOptions[]) {
    this.originalOptions = originalOptions;
    this.options = this.prepareOptions();
  }

  setNumberOfMedia(numberOfMedia: number) {
    this.numberOfMedia = numberOfMedia;
    this.options = this.prepareOptions();
  }

  getOptions(): NgxGalleryOptions[] {
    return this.options;
  }

  private prepareOptions(): NgxGalleryOptions[] {
    const optionsForLargeScreens = this.getOptionsForLargeScreens(this.originalOptions.find(option => !option.breakpoint) || {});
    const optionsForSmallScreens = this.getOptionsForSmallScreens(this.originalOptions.find(option => !!option.breakpoint) || {});

    return [
      optionsForLargeScreens,
      optionsForSmallScreens,
    ];
  }

  private getOptionsForLargeScreens(options: NgxGalleryOptions): NgxGalleryOptions {
    const optionsForLargeScreens: NgxGalleryOptions = {...this.defaultOptionsLargeScreen, ...options};

    // calc dimensions
    const boxWidth = parseFloat(optionsForLargeScreens.width);
    this.calcDimensions(boxWidth, optionsForLargeScreens.thumbnailsColumns);

    const customOptions = {
      width: this.dimensions.box.width + 'px',
      height: this.dimensions.box.height + 'px',
      ccpBoxLayout: this.galleryLayout,
      ccpImageWidth: this.dimensions.image.width,
      ccpImageHeight: this.dimensions.image.height,
      thumbnails: this.numberOfMedia > 1,
      thumbnailsPercent: this.getThumbnailsPercent(optionsForLargeScreens.thumbnailsColumns),
      thumbnailMargin: this.getThumbnailMargin(),
    };

    return {...customOptions, ...optionsForLargeScreens};
  }

  private getOptionsForSmallScreens(options: NgxGalleryOptions): NgxGalleryOptions {
    const optionsForSmallScreens: NgxGalleryOptions = {...this.defaultOptionsSmallScreen, ...options};

    const boxForSmallScreen = this.calcBoxForSmallScreens(optionsForSmallScreens.thumbnailsColumns);
    const customOptions = {
      width: boxForSmallScreen.width + 'px',
      height: boxForSmallScreen.height + 'px',
      image: this.numberOfMedia <= 1,
    }

    return {...optionsForSmallScreens, ...customOptions};
  }

  private calcDimensions(boxWidth: number, thumbnailColumns: number) {
    const boxAdjustment = 2 * this.boxPadding + 2 * this.boxBorder;
    const showThumbnails = this.numberOfMedia > 1;

    const box = {
      width: boxWidth,
      height: boxWidth,
    }

    const image = {
      width: box.width - boxAdjustment,
      height: box.height - boxAdjustment,
    }

    const thumbnail = {
      width: 0,
      height: 0,
    }

    if (showThumbnails) {
      thumbnail.width = Math.round((image.width - (thumbnailColumns - 1) * this.boxPadding) / thumbnailColumns);
      thumbnail.height = thumbnail.width;

      box.height = image.height + thumbnail.height + boxAdjustment + this.boxPadding;
    }

    this.dimensions = {
      box: box,
      image: image,
      thumbnail: thumbnail,
    }
  }

  private calcBoxForSmallScreens(thumbnailColumnsSmallScreen: number) {
    const showThumbnails = this.numberOfMedia > 1;

    if (showThumbnails) {
      return {
        height: this.dimensions.thumbnail.height + 2 * this.boxPadding + this.boxBorder, // not 2 * this.BoxBorder?
        width: this.dimensions.thumbnail.width * thumbnailColumnsSmallScreen
          + (1 + thumbnailColumnsSmallScreen) * this.boxPadding
          + 2 * this.boxBorder,
      };
    }

    return {
      width: this.dimensions.image.width,
      height: this.dimensions.image.height,
    }
  }

  private getThumbnailsPercent(thumbnailsColumns: number): number {
    return Math.round((100 - this.boxPadding) / thumbnailsColumns);
  }

  private getThumbnailMargin() {
    return this.boxPadding;
  }
}
