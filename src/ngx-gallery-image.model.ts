import { SafeResourceUrl } from '@angular/platform-browser';

import { VideoUrl } from './utils/videourl.model';
import { IMediumAddOn } from '@casus-campus/cc-support';

export interface INgxGalleryImage {
    id?: string;
    small?: string | SafeResourceUrl;
    medium?: string | SafeResourceUrl;
    big?: string | SafeResourceUrl;
    poster?: string | SafeResourceUrl;
    description?: string;
    url?: string;
    label?: string;
    type?: string;
    width?: number;
    height?: number;
    autoplay?: boolean;
    loop?: boolean;
    urls?: VideoUrl [];
    addOns?: IMediumAddOn[];
}

export class NgxGalleryImage implements INgxGalleryImage {
    id?: string;
    small?: string | SafeResourceUrl;
    medium?: string | SafeResourceUrl;
    big?: string | SafeResourceUrl;
    poster?: string | SafeResourceUrl;
    description?: string;
    url?: string;
    label?: string;
    type?: string;
    width?: number;
    height?: number;
    autoplay?: boolean;
    loop?: boolean;
    urls?: VideoUrl [];
    addOns?: IMediumAddOn[];

    constructor(obj: INgxGalleryImage) {
        this.id = obj.id;
        this.small = obj.small;
        this.medium = obj.medium;
        this.big = obj.big;
        this.poster = obj.poster;
        this.description = obj.description;
        this.url = obj.url;
        this.label = obj.label;
        this.type = obj.type;
        this.width = obj.width;
        this.height = obj.height;
        this.autoplay = obj.autoplay;
        this.loop = obj.loop;
        this.urls = obj.urls;
        this.addOns = obj.addOns;
    }
}
